package _02arrays;

import java.util.*;
import java.io.*;

public class P7_5 {

    public static void main(String[] args) throws IOException {

        Scanner scan = new Scanner(System.in);
        // User enter sample.csv
        System.out.println("Enter the input file: ");
        String str = scan.next();

        BufferedReader br = new BufferedReader(new FileReader(str));

        System.out.println("The total rows in the file are: " + numberOfRows(br));

        System.out.println("Enter the row to find the number of fields: ");
        int fields = numberOfFields(scan.nextInt(), br);
        System.out.println("The number of fields are: " + fields);

        System.out.println("Enter the row and column number: ");
        int row = scan.nextInt();
        int col = scan.nextInt();

        System.out.println("The corresponding field value is: " + field(row, col, br));

    }

    public static int numberOfRows(BufferedReader br) {

        int rows = 0;
        String line;
        while ((line = br.readLine()) != null) {
            rows++;
        }

        return rows;
    }

    public static int numberOfFields(int row, BufferedReader br) {

        int rows = 1;
        while (rows < row) {
            br.readLine();
            rows++;

        }

        int cols = 0;

        StringTokenizer str = new StringTokenizer(br.readLine(), ",");

        while (str.hasMoreElements()) {

            str.nextToken();
            cols++;

        }

        return cols;
    }

    public static String field(int row, int col, BufferedReader br) {

        int rows = 1;
        while (rows < row) {

            br.readLine();
            rows++;

        }

        int cols = 1;
        StringTokenizer str = new StringTokenizer(br.readLine(), ",");

        while (str.hasMoreElements() && cols < col) {

            str.nextToken();
            cols++;
        }

        return str.nextToken();

    }
}