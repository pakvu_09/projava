package _02arrays;

import java.util.*;

public class E6_1 {

    public static void main(String[] args) {

        Random rand = new Random();

        int[] arr = new int[10];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = rand.nextInt(1000);
        }

        System.out.println("Print every element at an even index: ");
        for (int i = 0; i < arr.length; i = i + 2) {
            System.out.println(arr[i]);

        }

        System.out.println("Print every even elements: ");
        for (int i = 0; i < arr.length; i++) {

            if (arr[i] % 2 == 0) {
                System.out.println(arr[i]);

            }

        }

        System.out.println("Print all elements in reverse order: ");
        for (int i = 9; i >= 0; i--) {

            System.out.println(arr[i]);

        }
        System.out.println("Print the first and last element: ");
        for (int i = 0; i < arr.length; i++) {

            if ((i == 0) || (i == 9)) {
                System.out.println(arr[i]);
            }

        }

    }
}
