package _02arrays;

import java.util.*;
import java.io.*;

public class E7_4 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        // User please enter input.txt and output.txt
        System.out.print("Enter input file name: ");
        String input_fileName = input.next();
        System.out.print("Enter output file name: ");
        String output_fileName = input.next();

        input.close();

        File inputFile = new File(input_fileName);
        Scanner in = null;
        File outputFile = new File(output_fileName);
        PrintWriter out = null;

        try {
            in = new Scanner(inputFile);
            out = new PrintWriter(outputFile);
        } catch (FileNotFoundException e1) {
            System.out.println("Files not found !!!");
        }

        int lineNumber = 1;
        while (in.hasNextLine()) {
            String line = in.nextLine();
            out.write(String.format("/* %d */ %s\n", lineNumber, line));
            lineNumber++;
        }

        out.close();
        in.close();

        System.out.println("Successfully read and write files!");
    }
}