package _02arrays;

import java.util.*;

public class E6_12 {
    public static void main(String[] args) {

        int[] arr = new int[20];
        Random rand = new Random();

        for (int i = 0; i < arr.length; i++) {

            arr[i] = rand.nextInt(100);

        }

        System.out.println("The original array is: ");
        System.out.println(Arrays.toString(arr));
        Arrays.sort(arr);
        System.out.println("The sorted array is: ");
        System.out.println(Arrays.toString(arr));

    }
}