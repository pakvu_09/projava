package _02arrays;

import java.util.*;

public class E6_16 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        // First we populate the matrix
        System.out.println("Please enter number of columns: ");

        int number_of_column = input.nextInt();
        int[] arr = new int[number_of_column];

        int max_height = 20;

        for (int i = 0; i < number_of_column; i++) {
            System.out.println("Please enter number of asterisk for column " + i);
            arr[i] = Math.min(input.nextInt(), max_height);
        }

        input.close();

        for (int j = 0; j < max_height; j++) {

            for (int i = 0; i < number_of_column; i++) {
                if (arr[i] >= max_height - j) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }

    }

}