package _02arrays;

import java.util.*;

public class E6_9 {

    public static void main(String[] args) {

        int[] a = { 1, 2, 3 };
        int[] b = { 1, 2, 3 };
        int[] c = { 1, 3, 3 };

        System.out.println("Array a and array b is equal: " + equals(a, b));
        System.out.println("Array a and array c is equal: " + equals(a, c));

    }

    public static boolean equals(int[] a, int[] b) {

        if (a.length != b.length) {

            return false;
        }

        for (int i = 0; i < a.length; i++) {
            if (a[i] != b[i]) {
                return false;
            }
        }

        return true;
    }
}