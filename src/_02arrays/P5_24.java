package _02arrays;

import java.util.*;

public class P5_24 {

    public static void main(String[] args) {

        String str = "MCMLXXVIII";

        int total = 0;

        while (str.length() > 0) {

            if ((str.length() == 1) || (convert_to_number(String.valueOf(str.charAt(0))) >= convert_to_number(
                    String.valueOf(str.charAt(1))))) {

                total += convert_to_number(String.valueOf(str.charAt(0)));
                str = str.substring(1);

            } else {

                int difference = convert_to_number(String.valueOf(str.charAt(1)))
                        - convert_to_number(String.valueOf(str.charAt(0)));
                total += difference;
                str = str.substring(1);
                str = str.substring(1);
            }

        }

        System.out.println(total);

    }

    public static int convert_to_number(String string) {

        Hashtable<String, String> my_dict = new Hashtable<String, String>();

        my_dict.put("I", "1");
        my_dict.put("V", "5");
        my_dict.put("X", "10");
        my_dict.put("L", "50");
        my_dict.put("C", "100");
        my_dict.put("D", "500");
        my_dict.put("M", "1000");

        String result = "";

        result = my_dict.get(string);

        return Integer.parseInt(result);

    }
}
