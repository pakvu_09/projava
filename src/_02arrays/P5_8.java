package _02arrays;

public class P5_8 {

    public static void main(String[] args) {

        int year = 2000;

        System.out.println("Year " + year + " is a leap year: " + isLeapYear(year));

    }

    public static boolean isLeapYear(int year) {

        boolean leap = false;

        if (year % 4 == 0) {
            if (year % 100 == 0) {
                // year is divisible by 400, hence the year is a leap year
                if (year % 400 == 0)
                    leap = true;
                else
                    leap = false;
            } else
                leap = true;
        } else
            leap = false;

        return leap;

    }

}