import java.util.*;

public class E3_14 {
    public static void main(String[] args) {

        System.out.println("Please enter the month and date: ");

        Scanner s = new Scanner(System.in);
        int month = s.nextInt();
        int date = s.nextInt();

        if ((month > 12) || (date > 31)) {
            System.out.println("Pls input a correct month and date");
            return;
        }

        String season = "";

        if (month == 1 | month == 2 | month == 3) {
            season = "Winter";

        } else if (month == 4 | month == 5 | month == 6) {
            season = "Spring";

        } else if (month == 7 | month == 8 | month == 9) {
            season = "Summer";

        } else {
            season = "Fall";

        }

        boolean isMonth_divisible_by_three = month % 3 == 0;

        if ((isMonth_divisible_by_three) && (date >= 21)) {

            if (season == "Winter") {
                season = "Spring";

            } else if (season == "Spring") {
                season = "Fall";
            } else if (season == "Fall") {
                season = "Summer";
            } else {
                season = "Winter";
            }
        }

        System.out.println("Season is: " + season);

    }

}