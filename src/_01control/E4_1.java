import java.util.*;

public class E4_1 {

    public static void main(String[] args) {

        System.out.println("The sum of all even numbers from 2 to 100 is: " + sum_evenNumber_two_to_hundred());
        System.out.println("The sum of all squared from 1 to 100 is: " + sum_squares_one_to_hundred());
        System.out.println("The sum of two to two to the power of 20 is: " + sum_power_of_two());
        sum_odd_number();
        sum_odd_digit();

    }

    public static int sum_evenNumber_two_to_hundred() {

        int start = 2;
        int end = 100;
        int current = start;
        int sum = 0;

        while (current <= end) {
            sum += current;
            current += 2;
        }
        return sum;
    }

    public static int sum_squares_one_to_hundred() {
        int start = 1;
        int end = 10;
        int current = start;
        int sum = 0;

        while (current <= end) {
            sum += current * current;
            current += 1;
        }
        return sum;
    }

    public static int sum_power_of_two() {
        int number = 2;
        int power = 0;

        int sum = 0;
        while (power <= 20) {

            sum += Math.pow(number, power);
            power = power + 1;
        }

        return sum;
    }

    public static void sum_odd_number() {

        System.out.println("Please enter two integers a and b: ");
        Scanner s = new Scanner(System.in);
        int a = s.nextInt();
        int b = s.nextInt();

        int current = a;

        if (current % 2 != 1) {
            current += 1;
        }

        int sum = 0;

        while (current <= b) {
            sum += current;
            current += 2;
        }

        System.out.println("The sum of all odd numbers from " + a + " to " + b + " is: " + sum);
    }

    public static void sum_odd_digit() {

        System.out.println("Please enter a random number: ");
        Scanner s = new Scanner(System.in);
        int number = s.nextInt();

        int sum = 0;
        int digit = 0;

        while (number > 0) {
            digit = number % 10; // we look at the "last" digit
            if (digit % 2 == 1) // odd digiti
            {
                sum += digit;
            }
            number /= 10;
        }

        System.out.println("The sum of all odd digits in the number is: " + sum);
    }
}