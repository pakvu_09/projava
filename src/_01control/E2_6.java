import java.util.Scanner;

public class E2_6 {

    public static void main(String[] args) {

        final double coefficient_to_miles = 0.000621371;
        final double coefficient_to_feet = 3.28084;
        final double coefficient_to_inches = 39.3701;

        Scanner in = new Scanner(System.in);

        System.out.println("Please enter a measurement in meters: ");

        double value = in.nextDouble();
        double value_in_miles = convert_measurement(value, coefficient_to_miles);
        double value_in_feet = convert_measurement(value, coefficient_to_feet);
        double value_in_inches = convert_measurement(value, coefficient_to_inches);

        System.out.println("The input measurement in miles is: " + value_in_miles);
        System.out.println("The input measurement in feet is: " + value_in_feet);
        System.out.println("The input measurement in inches is: " + value_in_inches);

    }

    public static double convert_measurement(double value, double coeffficient) {
        return value * coeffficient;
    }
}