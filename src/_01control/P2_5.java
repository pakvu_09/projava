import java.util.Scanner;

public class P2_5 {

    public static void main(String[] args) {

        Scanner s = new Scanner(System.in);

        System.out.println("Please enter a ten-digit phone number: ");

        String value = s.next();

        String convert_number = "";

        convert_number = "(" + value.substring(0, 3) + ")" + "-" + value.substring(3, 6) + "-" + value.substring(6, 10);
        System.out.println("Right format phone number: " + convert_number);

    }
}