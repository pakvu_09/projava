import java.util.Scanner;

public class E2_4 {

    public static void main(String[] args) {

        final double number = 2;

        System.out.println("Please enter two integer: ");

        Scanner s = new Scanner(System.in);
        int value_1 = s.nextInt();
        int value_2 = s.nextInt();
        int sum = value_1 + value_2;
        int difference = value_1 - value_2;
        int product = value_1 * value_2;
        double average = sum / number;
        int distance = Math.abs(difference);
        int max = Math.max(value_1, value_2);
        int min = Math.min(value_1, value_2);

        // Print output
        System.out.println("Sum:          " + sum);
        System.out.println("Difference:   " + difference);
        System.out.println("Product:      " + product);
        System.out.println("Average:      " + average);
        System.out.println("Distance:     " + distance);
        System.out.println("Max:          " + max);
        System.out.println("Min:          " + min);

    }
}