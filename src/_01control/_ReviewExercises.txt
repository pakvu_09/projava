#########################################################################
# Use this file to answer Review Exercises from the Big Java textbook
#########################################################################

R1.5 The following are all benefits to using Java over machine code:
1/ Java bytecode is platform independent and may be run on any system that has a JVM. This quality is known as portability.
2/ Java JIT compilers can run efficient Java programs as fast or faster than machine code programs
3/ Java is memory-managed. This reduces about 95% of runtime errors (mostly memory-related) as compared to unmanaged code.
4/ Java was designed for networking and the Internet in mind; and it is generally safer than machine code
5/ With the introduction of Java8, Java is now a declarative and functional programming language.

###-------------------------------------------###
R2.4 Translating Math experssions to Java
###-------------------------------------------###

s = s_nought + v_nought * t + (1/2)*g*(t*t)
FV = PV * Math.pow((1 + INT / 100), YRS)
G = 4 * Math.pow(Math.PI,2) * Math.pow(a,3) / (p * p * (m1 + m2))
c = Math.sqrt(a * a + b * b - 2 * a * b * Math.cos(Math.toRadians(gamma)))

###-------------------------------------------###
R2.7 Calculate the values of the following expressions
###-------------------------------------------###

a. 1 + 7 = 8
b. 1 + 0 = 1
c. (int)= 17
d. (double)= 17.5
e. 17
f. 18



###-------------------------------------------###
R2.14 Explain the difference
###-------------------------------------------###

 2 is a value of type int, 2.0 is a value of type double,
 '2' and “2” are both values of type String with one character 
 “2.0” is a value of type String with 3 characters (a 2, a . and a 0).


###-------------------------------------------###
R2.17 Write pseudocode
###-------------------------------------------###

Step 1: Read the name by character
Step 2: Create an array to store letters
Step 3: Assign the first letter to the array
Step 4: Loop through all characters of the name
Step 5: If encounter a space, assign the next character to the array
Step 6: Read the array

###-------------------------------------------###
R2.22 More pseudocode
###-------------------------------------------###

index = 3 * given_day;
string = days[index]

string += days[index  + 1];
string += days[index  + 2];

return string

###-------------------------------------------###
R3.19 Pseudocode for grade problem
###-------------------------------------------###

Read score

if score >= 90 and score <= 100:
    grade = A

elif score >= 80 and score < 90:
    grade = B

elif score >= 70 and score < 80:
    grade = C

elif score >= 60 and score < 70:
    grade = D
else
    grade = F

###-------------------------------------------###
R3.30 Values of the following expressions
###-------------------------------------------###

a. False
b. True
c. True
d. True
e. False
f. False
g. False
h. True

###-------------------------------------------###
R4.12 Loops
###-------------------------------------------###

Java supports three types of loop statements: for, do, and while.
Use a for loop if you know in advance how many times the loop should be repeated.
Use a do loop if the loop must be executed at least once. Otherwise, use a while loop.


###-------------------------------------------###
R4.14 Calendar problems
###-------------------------------------------###


read two input:
    input_1 = day_of_week; (where the first day of the month start)
                            0: Sunday and 6: Saturday
    input_2 = days_in_Month (28, 29, 30 or 31)

Print header line: Su Mon .... Sat
Print new line
Loop through input_1 + input_2:

    if index < input_1: print empty string
    if index > input_1: print date

    if index divisible for 7, print new line