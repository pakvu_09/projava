import java.util.*;

public class P3_7 {

    public static void main(String[] args) {

        System.out.println("Please enter your income: ");

        Scanner s = new Scanner(System.in);
        double income = s.nextDouble();

        double tax_income = 0;

        final double Income_Bracket_One = 50000;
        final double Income_Bracket_Two = 75000;
        final double Income_Bracket_Three = 100000;
        final double Income_Bracket_Four = 250000;
        final double Income_Bracket_Five = 500000;

        final double Tax_Percent_Bracket_One = 0.01;
        final double Tax_Percent_Bracket_Two = 0.02;
        final double Tax_Percent_Bracket_Three = 0.03;
        final double Tax_Percent_Bracket_Four = 0.04;
        final double Tax_Percent_Bracket_Five = 0.05;
        final double Tax_Percent_Bracket_Six = 0.06;

        double Income_tax_bracket_one = Income_Bracket_One * Tax_Percent_Bracket_One;
        double Income_tax_bracket_two = Income_tax_bracket_one
                + (Income_Bracket_Two - Income_Bracket_One) * Tax_Percent_Bracket_Two;
        double Income_tax_bracket_three = Income_tax_bracket_two
                + (Income_Bracket_Three - Income_Bracket_Two) * Tax_Percent_Bracket_Three;
        double Income_tax_bracket_four = Income_tax_bracket_three
                + (Income_Bracket_Four - Income_Bracket_Three) * Tax_Percent_Bracket_Four;
        double Income_tax_bracket_five = Income_tax_bracket_four
                + (Income_Bracket_Five - Income_Bracket_Four) * Tax_Percent_Bracket_Five;

        if (income <= Income_Bracket_One) {
            tax_income = income * Tax_Percent_Bracket_One;

        } else if (income <= Income_Bracket_Two) {
            tax_income = (income - Income_Bracket_One) * Tax_Percent_Bracket_Two + Income_tax_bracket_one;
        } else if (income <= Income_Bracket_Three) {
            tax_income = (income - Income_Bracket_Two) * Tax_Percent_Bracket_Three + Income_tax_bracket_two;

        } else if (income <= Income_Bracket_Four) {
            tax_income = (income - Income_Bracket_Three) * Tax_Percent_Bracket_Four + Income_tax_bracket_three;

        } else if (income <= Income_Bracket_Four) {
            tax_income = (income - Income_Bracket_Four) * Tax_Percent_Bracket_Five + Income_tax_bracket_four;

        } else {
            tax_income = (income - Income_Bracket_Five) * Tax_Percent_Bracket_Six + Income_tax_bracket_five;
        }

        System.out.println("Your income tax is: " + tax_income);

    }
}