import java.util.*;

public class E4_17 {

    public static void main(String[] args) {

        System.out.println("Please enter a random number: ");
        Scanner s = new Scanner(System.in);
        int number = s.nextInt();

        convert_to_binary(number);

    }

    public static void convert_to_binary(int num) {
        int binary[] = new int[10];
        int index = 0;
        while (num > 0) {
            binary[index++] = num % 2;
            num = num / 2;
        }
        for (int i = 0; i < index; i++) {
            System.out.println(binary[i]);
        }
    }
}