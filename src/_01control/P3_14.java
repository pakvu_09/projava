import java.util.*;

public class P3_14 {

    public static void main(String[] args) {

        System.out.println("Please enter a random year: ");

        Scanner s = new Scanner(System.in);
        int year = s.nextInt();

        boolean leap = false;

        if (year % 4 == 0) {
            if (year % 100 == 0) {
                // year is divisible by 400, hence the year is a leap year
                if (year % 400 == 0)
                    leap = true;
                else
                    leap = false;
            } else
                leap = true;
        } else
            leap = false;

        if (leap)
            System.out.println(year + " is a leap year.");
        else
            System.out.println(year + " is not a leap year.");

    }
}