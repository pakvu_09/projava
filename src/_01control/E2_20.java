public class E2_20 {

    public static void main(String[] args) {

        // Size body should be larger than 3 for beautiful visual effect
        int size_body = 5;
        int size_leg = 3;
        draw_tree(size_body, size_leg);

    }

    public static void draw_tree(int size_body, int size_leg) {

        int height = size_body;
        int width = size_body;

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width - i; j++) {
                System.out.print(" ");
            }
            System.out.print("/");
            for (int k = 0; k < 2 * i; k++) {
                System.out.print(" ");
            }
            System.out.print("\\");
            System.out.println();
        }
        System.out.print(" ");
        for (int j = 0; j < 2 * width; j++) {
            System.out.print("-");
        }
        System.out.println();

        for (int i = 0; i < size_leg; i++) {
            for (int j = 0; j < 4; j++) {
                System.out.print(" ");
            }
            System.out.print("|");
            for (int j = 0; j < 2 * width - 4 * 2; j++) {
                System.out.print(" ");
            }
            System.out.print("|");
            System.out.println();
        }

    }
}