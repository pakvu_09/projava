package _03objects.P8_1;

public class Microwave {

    private int time;
    private int level;

    public Microwave() {

        time = 0;
        level = 1;
    }

    public int getTime() {

        return time;
    }

    public void increase_time_30s() {

        time += 30;

    }

    public void switch_power() {

        if (level == 1) {
            level = 2;
        } else {
            level = 1;
        }
    }

    public void reset() {
        time = 0;
        level = 1;
    }

    public void start() {

        System.out.println("Cooking for " + time + " seconds at level " + level);
    }

    public static void main(String[] args) {

        Microwave new_microwave = new Microwave();

        new_microwave.increase_time_30s();
        new_microwave.increase_time_30s();
        new_microwave.increase_time_30s();
        new_microwave.increase_time_30s();

        new_microwave.start();
        new_microwave.start();
        new_microwave.reset();
        new_microwave.switch_power();
        new_microwave.start();
    }
}
