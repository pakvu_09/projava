#########################################################################
# Use this file to answer Review Exercises from the Big Java textbook
#########################################################################


------------------------R8.1 Encapsulation---------------------------

Encapsulation is used to hide the values or state of a structured data object
inside a class, preventing unauthorized parties' direct access to them.

Encapsulation is important as it helps in isolating implementation details from the
behavior exposed to clients of a class (other classes/functions that are using
this class), and gives you more control over coupling in your code. The fewer
dependencies there are in the code the more flexible and easier to maintain.


--------------------R8.4 Public interface--------------------------

The public interface specifies the functionality supported by the class but
does not disclose any details of how the functionality is implemented.
In contrast, the implementation of a class is the code that accomplishes
the tasks to support the class's functionality.

----------------R8.7 Instance versus static-------------------------

Instance method are methods which require an object of its class to be create
before it can be called. Static methods are the methods in Java that can be
called without creating an object of class.

-------------R8.8 Mutator and accessor------------------------------

"Accessor" and "Mutator" are just fancy names fot a getter and a setter.
A getter, "Accessor", returns a class's variable or its value.
A setter, "Mutator", sets a class variable pointer or its value.


--------------------R8.9 Implicit parameter----------------------------

The implicit parameter in Java is the object that the method belongs to.
It's passed by specifying the reference or variable of the object before the
name of the method. An implicit parameter is opposite to an explicit parameter,
which is passed when specifying the parameter in the parenthesis of a method call.
If a parameter isn't explicitly defined, the parameter is considered implicit.


Explicit parameters are parameters explicitly mentioned in method declaration


--------------------R8.10 Implicit parameter----------------------------

An instance method can have 1 implicit parameter.

A static method has zero implicit parameter.

An instance method can have many explicit parameters. 

--------------------------R8.12 Constructors------------------------------

A class can have multiple constructors. 
You can not have a class without constructor. If no constructor is provided,
a constructor with no parameters is generated automatically by compiler.
If a class has more than one constructor, the compiler picks the one that
matches the construction parameters.

If we do not mention any arguments the default constructor get called.


------------------------R8.16 Instance variables-------------------------

Explain to what extent the private reserved word hides the private
implementation of a class.

Instance variables are made private to force the users of those class to use methods
to access them. In most cases there are plain getters and setters but other methods
might be used as well. Using methods would allow you, for instance, to restrict
access to read only, i.e. a field might be read but not written, if there's no setter.
That would not be possible if the field was public.

Additionally, you might add some checks or conversions for the field access,
which would not be possible with plain access to a public field. If a field was
public and you'd later like to force all access through some method that performs
additional checks etc. You'd have to change all usages of that field.
If you make it private, you'd just have to change the access methods later on.

------------------------R8.19 The this reference---------------------------

The this is a keyword in Java which is used as a reference to the object
of the current class, with in an instance method or a constructor.
Using this you can refer the members of a class such as constructors,
variables and methods.

----------------R8.20 Zero, null, false, empty String------------------------

zero is an integer with value = 0, false is boolean type.

 An empty string is a string instance of zero length,
 whereas a null string reference has no value at all.