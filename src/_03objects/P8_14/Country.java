package _03objects.P8_14;

public class Country {

    private String name;
    private double area;
    private double population;
    private double density;

    public Country(String name, double area, double population) {
        this.name = name;
        // square miles
        this.area = area;

        // million
        this.population = population;

        density = population / area;
    }

    public String getName() {

        return name;
    }

    public double getArea() {

        return area;
    }

    public double getPopulation() {

        return population;
    }

    public double getDensity() {

        return density;
    }

    public static void largest_area(Country[] args) {

        String country_name = "";
        double area = 0;

        // iterate through the country list, if find the country with larger area,
        // swap the largest area and name with that country
        for (int i = 0; i < args.length; i++) {

            if (area < args[i].getArea()) {

                area = args[i].getArea();
                country_name = args[i].getName();
            }
        }

        System.out.println(country_name);
    }

    public static void largest_population(Country[] args) {

        String country_name = "";
        double population = 0;

        // iterate through the country list, if find the country with larger population,
        // swap the largest area and name with that country

        for (int i = 0; i < args.length; i++) {

            if (population < args[i].getPopulation()) {

                population = args[i].getPopulation();
                country_name = args[i].getName();
            }
        }

        System.out.println(country_name);
    }

    public static void largest_density(Country[] args) {

        String country_name = "";
        double density = 0;

        // iterate through the country list, if find the country with larger density,
        // swap the largest area and name with that country

        for (int i = 0; i < args.length; i++) {

            if (density < args[i].getDensity()) {

                density = args[i].getDensity();
                country_name = args[i].getName();
            }
        }

        System.out.println(country_name);
    }

    public static void main(String[] args) {

        Country USA = new Country("USA", 10000, 300);
        Country CANADA = new Country("CANADA", 30000, 100);
        Country MEXICO = new Country("MEXICO", 5000, 299);

        Country[] countries = new Country[3];
        countries[0] = USA;
        countries[1] = CANADA;
        countries[2] = MEXICO;

        largest_area(countries);
        largest_population(countries);
        largest_density(countries);
    }

}