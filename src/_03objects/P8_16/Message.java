package _03objects.P8_16;

public class Message {

    private String recipient;
    private String sender;
    private String message_text;

    public Message(String recipient, String sender) {

        this.recipient = recipient;
        this.sender = sender;
        message_text = "";
    }

    public void append(String text_message) {
        message_text += text_message + "\n";
    }

    public String toString() {

        return "From: " + sender + "\n" + "To: " + recipient + "\n" + message_text;
    }
}