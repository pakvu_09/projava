package _03objects.P8_16;

import java.util.*;

public class Mailbox {

    // Use ArrayList as it can grow and shrink dynamically, retrieve and remove item
    // using index

    private List<Message> messages = new ArrayList<Message>();

    public void addMessage(Message message) {
        messages.add(message);

    }

    public Message getMessage(int i) {
        return messages.get(i);
    }

    public void removeMessage(int i) {
        messages.remove(i);
    }

    public static void main(String[] args) {

        Message message1 = new Message("Adam", "Eva");

        message1.append("Aloha");

        Mailbox mail_box = new Mailbox();

        mail_box.addMessage(message1);
        System.out.println(mail_box.getMessage(0).toString());

    }

}
