package _03objects.P8_7;

public class ComboLock {

    private int secret1;
    private int secret2;
    private int secret3;

    private boolean result = false;

    private int dial;

    // current_state = 0: turning left the first time
    // current_state = 1: already turn left once
    // current_state = 2: already turn left and right once, turning left one more
    // time
    private int current_state = 0;

    private int[] combos = new int[3];

    public ComboLock(int secret1, int secret2, int secret3) {

        this.secret1 = secret1;
        this.secret2 = secret2;
        this.secret3 = secret3;
    }

    public void reset() {

        combos[current_state] = dial;

        if (current_state < 2) {
            current_state += 1;
        } else {
            current_state = 0;
        }

        dial = 0;

    }

    public void turnLeft(int ticks) {

        dial += ticks;

    }

    public void turnRight(int ticks) {

        dial += ticks;

    }

    public boolean open() {

        combos[current_state] = dial;

        // the lock opens when the user turns the dial correctly three times
        result = ((combos[0] == secret1) && (combos[1] == secret2) && (combos[2] == secret3));

        return result;

    }

    public static void main(String[] args) {

        ComboLock lock = new ComboLock(10, 15, 20);
        lock.turnLeft(10);
        lock.reset();
        lock.turnRight(15);
        lock.reset();

        // 15 + 5 = 20 so the password is correct
        lock.turnLeft(15);
        lock.turnLeft(5);

        if (lock.open()) {
            System.out.println("Lock is opened.");
        } else {
            System.out.println("Lock is closed !");
        }
        ;

    }

}
