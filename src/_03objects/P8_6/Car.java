package _03objects.P8_6;

public class Car {

    // Unit: miles per gallon
    private double fuel_efficiency;
    private double fuel;

    public Car(double fuel_efficiency) {
        this.fuel_efficiency = fuel_efficiency;
        fuel = 0;
    }

    public double getGasLevel() {
        return fuel;
    }

    public void addGas(double gas) {
        fuel += gas;
    }

    public void drive(double distance) {

        if (distance <= fuel_efficiency * fuel) {

            fuel -= distance / fuel_efficiency;
        } else {

            System.out.println("Running out of gas");
            fuel = 0;
        }

    }

    public static void main(String[] args) {

        Car myHybrid = new Car(50); // 50 miles per gallon
        myHybrid.addGas(20); // Tank 20 gallons
        myHybrid.drive(100000); // Drive 100 miles
        System.out.println(myHybrid.getGasLevel()); // Print fuel remaining

    }
}
