package _03objects.P8_5;

public class Soda_can {

    private final double pi = 3.14;
    private final int two = 2;

    private int height;
    private int radius;
    private double surface_area;
    private double volume;

    public Soda_can() {

        height = 0;
        radius = 0;
    }

    public Soda_can(int h, int r) {

        height = h;
        radius = r;
    }

    public double getSurfaceArea() {

        surface_area = two * radius * pi * height + radius * radius * pi * two;

        return surface_area;

    }

    public double getVolume() {

        volume = radius * radius * pi * height;

        return volume;

    }

    public static void main(String[] args) {

        Soda_can new_can = new Soda_can(5, 15);

        System.out.println("The surface area of the can is: " + new_can.getSurfaceArea());
        System.out.println("The volume of the can is: " + new_can.getVolume());

    }

}