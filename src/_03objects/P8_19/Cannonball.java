package _03objects.P8_19;

class Cannonball {

    private double x_position;
    private double y_position;
    private double x_velocity;
    private double y_velocity;

    // Assign constant
    private final double g = -9.81;
    private final double half = 0.5;

    public Cannonball(double x_position) {

        this.x_position = x_position;
        y_position = 0;

    }

    public void move(double sec) {

        double x_distance = x_velocity * sec;
        double y_distance = y_velocity * sec + half * g * sec * sec;

        x_position += x_distance;
        y_position += y_distance;

        y_velocity += g * sec;

    }

    public double getX() {

        return x_position;
    }

    public double getY() {

        return y_position;
    }

    public void shoot(double angle, double initial_velocity) {
        // angle is in degree, convert into radians
        x_velocity = initial_velocity * Math.cos(Math.toRadians(angle));
        y_velocity = initial_velocity * Math.sin(Math.toRadians(angle));

        // iterate until y_position < 0 (contact with the ground)
        while (y_position >= 0) {

            this.move(0.1);
            System.out.println("The x and y position of the ball is: " + this.getX() + " , " + this.getY());

        }
    }

    public static void main(String[] args) {

        Cannonball ball = new Cannonball(0);

        ball.shoot(45, 20);
    }

}