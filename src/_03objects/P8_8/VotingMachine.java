package _03objects.P8_8;

public class VotingMachine {

    int democrate_votes;
    int republican_votes;

    public void vote_democrats() {

        democrate_votes += 1;
    }

    public void vote_republicans() {

        republican_votes += 1;
    }

    public void reset() {

        democrate_votes = 0;
        republican_votes = 0;
    }

    public void get_tallies() {

        System.out.println("The total number of votes for Democrats and Republicans are: " + democrate_votes + " and "
                + republican_votes + ". Biden is a new president.");
    }

    public static void main(String[] args) {

        VotingMachine machine = new VotingMachine();

        for (int i = 0; i < 270; i++) {
            machine.vote_democrats();

        }

        for (int i = 0; i < 100; i++) {
            machine.vote_republicans();

        }

        machine.get_tallies();

    }
}