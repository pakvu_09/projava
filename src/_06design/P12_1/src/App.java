import java.util.*;

public class App {
    public static void main(String[] args) {


        VendingMachine machine = new VendingMachine(); 


        // add some coins into vending machine in advance
        machine.addCoins(new Coin(0.01), 200);
        machine.addCoins(new Coin(0.05), 100);
        machine.addCoins(new Coin(0.10), 20);
        machine.addCoins(new Coin(0.25), 10);


        // add some products into vending machine in advance
        machine.addProducts(new Product("Coke",0.75), 15);
        machine.addProducts(new Product("Pepsi",0.75), 15);
        machine.addProducts(new Product("Snicker",1.25), 15);
        machine.addProducts(new Product("Kitkat",1.0), 15);
        machine.addProducts(new Product("Cheetos",1.5), 15);


        while (true) {
            // print the selection panel
            System.out.println("Please take action:");
            System.out.println("1. Add coins to machine; 2. Add products (restock); 3. Remove coins from machine");
            System.out.println("4. Remove products from machine; 5. Purchase product; 6. Get vending machine info");
            System.out.println("0. Exit");
            System.out.println("Please Enter (0~6):");
            Scanner scanner = new Scanner(System.in);
            int step = scanner.nextInt();
            if (step == 0){
                break;
            }
            else if (step == 1){
                // add coins
                boolean flag1 = true;
                while(flag1) {
                    System.out.println("Coin Value? ");
                    double value = scanner.nextDouble();
                    Coin cn = new Coin(value);
                    System.out.println("How many coins do you want to add? ");
                    int number = scanner.nextInt();
                    machine.addCoins(cn, number);
                    System.out.println("Continue? (y/n)");
                    String s = scanner.next();
                    if(s.equals("n")){
                        flag1 = false;
                    }
                }

            }
            else if (step == 2){
                // add products
                boolean flag2 = true;
                while(flag2) {
                    System.out.println("Product Name?");
                    String name = scanner.next();
                    System.out.println("Product price?");
                    double price = scanner.nextDouble();
                    Product p = new Product(name,price);
                    System.out.println("How many products do you want to add? ");
                    int number = scanner.nextInt();
                    machine.addProducts(p, number);
                    System.out.println("Continue? (y/n)");
                    String s = scanner.next();
                    if(s.equals("n")){
                        flag2 = false;
                    }
                }
            }
            else if (step == 3) {
                // remove coins
                boolean flag3 = true;
                while (flag3) {
                    String info = machine.toString();
                    System.out.println(info);
                    System.out.println("Coin value?");
                    double value = scanner.nextDouble();
                    Coin cn = new Coin(value);
                    if (!machine.hasCoin(cn)){
                        // if do not have such coin
                        System.out.println("Do not have such coin.");
                        continue;
                    }
                    System.out.println("How many coins do you want to remove? ");
                    int number = scanner.nextInt();
                    if (machine.getNumCoin(cn) < number){
                        // if do not have enough coins
                        System.out.println("Do not have that many coins.");
                        continue;
                    }
                    machine.removeCoins(cn,number);
                    System.out.println("Successfully remove coins");
                    System.out.println("Continue? (y/n)");
                    String s = scanner.next();
                    if (s.equals("n")) {
                        flag3 = false;
                    }
                }
            }
            else if (step == 4) {
                // remove products
                boolean flag4 = true;
                while (flag4) {
                    // show the vending machine coin and product stock
                    String info = machine.toString();
                    System.out.println(info);
                    System.out.println("Product name?");
                    String name = scanner.next();
                    if (!machine.hasProduct(name)){
                        // if do not have such product
                        System.out.println("Do not have such product.");
                        continue;
                    }
                    System.out.println("How many items do you want to withdraw? ");
                    int number = scanner.nextInt();
                    if (machine.getNumProduct(name) < number){
                        // if do not have enough products
                        System.out.println("Do not have so many products.");
                        continue;
                    }
                    machine.removeProducts(name,number);
                    System.out.println("Continue? (y/n)");
                    String s = scanner.next();
                    if (s.equals("n")) {
                        flag4 = false;
                    }
                }
            }
            else if (step == 5) {
                // make purchase

                // show the vending machine coin and product stock
                String info = machine.toString();
                System.out.println(info);
                System.out.println("Type the product you want (ex. Kitkat) ?");
                String product_name = scanner.next();
                if(!machine.hasProduct(product_name)){
                    System.out.println("Do not have such product.");
                    continue;
                }
                double totalMoneyNeed = machine.getPriceProduct(product_name);
                System.out.println("Total money needed: " + totalMoneyNeed);
                // System.out.println("Please insert coins: ");
                System.out.println("(Note the machine does not give change ^^)");
                HashMap<Double,Integer> currCoins = new HashMap<Double, Integer>(); // save the coins inserted
                double currValue = 0; // record the value of current inserted coins
                boolean flag5 = true;
                while(flag5){
                    // keep inserting coins
                    System.out.println("Please enter coin value: 0.01, 0.05, 0.1 or 0.25$");
                    double value = scanner.nextDouble();
                    System.out.println("Please enter number of coins");
                    int coinNumber = scanner.nextInt();
                    
                    currValue += value * coinNumber;
                    Coin coin = new Coin(value);
                    
                    if(currCoins.containsKey(coin.getValue())){
                        currCoins.put(coin.getValue(), currCoins.get(coin.getValue())+ coinNumber);
                    }
                    else{
                        currCoins.put(coin.getValue(), coinNumber);
                    }
                    System.out.println("Current value: " + currValue);
                    System.out.println("Continue? (y/n)");
                    String s = scanner.next();
                    if (s.equals("n")) {
                        flag5 = false;
                    }
                }
                
                if (currValue < totalMoneyNeed){
                    System.out.println("Not enough money!");
                    System.out.println("Returning the change: " + (currValue));
                    // Coin change = new Coin(currValue);
                    // machine.removeCoins(change,1); // remove coin for change
                    for (Map.Entry<Double,Integer> pair: currCoins.entrySet()) {
                        machine.removeCoins(new Coin(pair.getKey()),pair.getValue());// add coins to machine
                    }
                }
                else if ( currValue - totalMoneyNeed >= 0){
                    // successful purchase
                    System.out.println("Successful purchase!");
                    if (currValue - totalMoneyNeed> 0) {
                        System.out.println("Thanks for tips:" + (currValue - totalMoneyNeed));
                
                    }
                    machine.removeProducts(product_name,1); // remove purchased product
                    for (Map.Entry<Double,Integer> pair: currCoins.entrySet()) {
                        machine.addCoins(new Coin(pair.getKey()),pair.getValue());// add coins to machine
                    }
                }
     
            }
            else if (step == 6){
                // show the vending machine info
                String info = machine.toString();
                System.out.println(info);
            }

        }

    }
}

