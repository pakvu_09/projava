import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VendingMachine {

    // Use datastructure HashMap for constant time look up

    // coins  = A key-value pair of coin value and number of coins
    // ex: {5, 25} means there are 25 nickels
    HashMap<Double,Integer> coins;

    // products = A pair of product name and an array including product price, number of products
    // As a list can not contains two different types of data, I opt for Object
    // Then I convert object back to double (price) and int(number of products)
    HashMap<String, List<Object>> products; 

    public VendingMachine(){
        coins = new HashMap<Double,Integer>();
        products =new HashMap<String, List<Object>>();
    }

    public void addCoins(Coin coin, int coin_number){
        if(coins.containsKey(coin.getValue())){
            coins.put(coin.getValue(), coins.get(coin.getValue()) + coin_number);
        }
        else{
            coins.put(coin.getValue(), coin_number);
        }
    }

    public void removeCoins(Coin coin, int coin_number){
        if(coins.containsKey(coin.getValue())){
            coins.put(coin.getValue(), coins.get(coin.getValue()) - coin_number);
        }
        if(coins.get(coin.getValue()) == 0){
            // if the number is 0, remove that coin
            coins.remove(coin.getValue());
        }
    }

    public void addProducts(Product product, int number){

        // Create a list of two elements, price and number of products
        List<Object> price_num =  new ArrayList<Object>();
        price_num.add(product.getPrice());
        price_num.add(number);

        if(products.containsKey(product.getName())){
            products.put(product.getName(), price_num);
        }
        else{
            products.put(product.getName(), price_num);
        }
    }

    public void removeProducts(String product, int number){

        if(products.containsKey(product)){
            List<Object> price_num = products.get(product);
            int current_number = (int) price_num.get(1); 
            price_num.set(1, current_number-number);
            products.put(product, price_num);
        }

        int current_number = (int) products.get(product).get(1);
        if(current_number == 0){
            // if the number is 0, remove that product
            products.remove(product);
        }
    }

    public boolean hasCoin(Coin coin){
        // if machine have such coin
        return coins.containsKey(coin.getValue());
    }

    public boolean hasProduct(String product){
        // if machine have such product
        return products.containsKey(product);
    }

    public int getNumCoin(Coin coin){
        // return the number of coins
        return coins.get(coin.getValue());
    }

    public int getNumProduct(String product){
        // return the number products
        int product_number = (int) products.get(product).get(1);
        return product_number;
    }

    public double getPriceProduct(String product){
        // return the price of that product
        double product_price = (double) products.get(product).get(0);
        return product_price;
    }
    @Override

    public String toString(){
        // create the string for vending machine information
        String s = "";
        for (Map.Entry<Double, Integer> pair: coins.entrySet()) {
            s += "coin " + pair.getKey() +" $: " +"amount available " + pair.getValue() +" coins " + "\n";
        }

        for (Map.Entry<String, List<Object>> pair: products.entrySet()) {
            s += "product " + pair.getKey() +" price: $" + pair.getValue().get(0) +" remaining " + pair.getValue().get(1) + "\n";
        }
        return s;
    }
}
