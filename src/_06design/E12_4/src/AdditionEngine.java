import java.util.ArrayList;
import java.util.Random;

public class AdditionEngine {

    // Pseudocode: 
    // Choose a random target number
    // For level 1, target number is chosen to be smaller than 10 
    // For level 2, target number is smaller than 20 (to increase difficulty increases this number)
    // Recursively generate list of intergers that sum up to chosen target number
    // Return list of integers

    private int targetSum;

    public AdditionEngine(int targetSum){
        this.targetSum = targetSum; // read a target sum
    }

    public ArrayList<Integer> generate(){

        ArrayList<Integer> integers = new ArrayList<Integer>(0); // generate integers add up to target

        if (targetSum == 0) {

            integers.add(0);
            
        } else {

            Random rand = new Random(); ;// randomly generator
            while(targetSum > 0){
    
                int curr = rand.nextInt(Math.min(targetSum+1,9));
                targetSum -= curr;
                integers.add(curr);
            }

        }       

        return integers;
    }
}

