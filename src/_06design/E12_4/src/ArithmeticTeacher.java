import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class ArithmeticTeacher {
    
    private int score;
    private int num_of_attempts;
    private boolean game_over;

    public ArithmeticTeacher(){
        
        num_of_attempts = 0;
        score = 0;
        game_over = false;
    }

    public void teach(){
        Random rnd = new Random();

        System.out.println("Starting level 1:");
        while(score < 5){
            // level 1
            int targetSum = rnd.nextInt(10);
            AdditionEngine addG = new AdditionEngine(targetSum);
            ArrayList<Integer> integers = addG.generate(); // generate a set of numbers
            String s = "";
            for(int i = 0;i < integers.size();i++){
                s += Integer.toString(integers.get(i));
                if(i < integers.size()-1){
                    s += " + ";
                }
            }
            System.out.println(s + " = ?");
            Scanner scanner = new Scanner(System.in);

            // Get the users input
            int ans = scanner.nextInt(); 
          

            // Check if users' answers are correct
            if(ans == targetSum){
                score += 1;
                System.out.println("Correct!");
                System.out.println("Current score: " + score);
            }
            else{
                if (num_of_attempts == 0) {

                    System.out.println("Wrong! You have one more try");
                    System.out.println(s + " = ?");

                    // Get the users input
                    int second_ans = scanner.nextInt(); 

                    if(second_ans == targetSum){
                        score += 1;
                        System.out.println("Correct!");
                        System.out.println("Current score: " + score);
                    } else {
                        System.out.println("Current score: " + score);
                        System.out.println("You have used all attempts. Game over");
                        game_over = true;

                        break;
                    }   
                }
            }
        }

        // Game continue if players pass level 1, else bypass this step
        if (game_over == false) {

            System.out.println("You have cleared Level 1!");
            System.out.println("Going to Level 2:");
            score = 0;

            while(score < 5){
                // level 2
                int targetSum = rnd.nextInt(20);
                AdditionEngine addG = new AdditionEngine(targetSum);
                ArrayList<Integer> integers = addG.generate(); // generate a set of nunmbers
                String s = "";
                for(int i = 0; i < integers.size(); i++){
                    s += Integer.toString(integers.get(i));
                    if(i < integers.size()-1){
                        s += " + ";
                    }
                }
                System.out.println(s + " = ?");
                Scanner scanner = new Scanner(System.in);
                int ans = scanner.nextInt(); // get the student's answer
           
                if(ans == targetSum){
                    score += 1;
                    System.out.println("Correct!");
                    System.out.println("Current score: " + score);
                }
                else{
                    if (num_of_attempts == 0) {

                        System.out.println("Wrong! You have one more try");
                        System.out.println(s + " = ?");
    

                        // Get the users input
                        int second_ans = scanner.nextInt(); 
    
                        if(second_ans == targetSum){
                            score += 1;
                            System.out.println("Correct!");
                            System.out.println("Current score: " + score);
                        } else {
                            System.out.println("Current score: " + score);
                            System.out.println("You have used all attempts. Game over");
                            game_over = true;
    
                            break;
    
                        }    
    
                    }
                }
            }           
        }

        // Game continue if players pass level 1, else bypass this step
        if (game_over == false) {

            System.out.println("You have cleared Level 2!");
            System.out.println("Going to Level 3:");
            score = 0;
    
            while(score < 5){
                // level 3

                // Programming idea:
                // Choose a random starting number < 10:
                // Choose a random remainder < starting number 
                // Calculate the difference between starting number and remainder
                // Using AdditionEngine to generate a list of numbers whose sum is equal to the difference

                int startingNumber = rnd.nextInt(10);
                int remainder = rnd.nextInt(startingNumber + 1);
                int difference = startingNumber - remainder;


                AdditionEngine addG = new AdditionEngine(difference);
                ArrayList<Integer> integers = addG.generate(); // generate a set of numbers

                String s = Integer.toString(startingNumber);
                for(int i = 0;i < integers.size();i++){
                    s += " - " + integers.get(i);
                }
                System.out.println(s + " = ?");

                Scanner scanner = new Scanner(System.in);
                int ans = scanner.nextInt(); // get the student's answer
     
                if(ans == remainder){
                    score += 1;
                    System.out.println("Correct!");
                    System.out.println("Current score: " + score);
                }
                else{
                    if (num_of_attempts == 0) {

                        System.out.println("Wrong! You have one more try");
                        System.out.println(s + " = ?");
    
                        // Get the users input
                        int second_ans = scanner.nextInt(); 
    
                        if(second_ans == remainder){
                            score += 1;
                            System.out.println("Correct!");
                            System.out.println("Current score: " + score);
                        } else {
                            System.out.println("Current score: " + score);
                            System.out.println("You have used all attempts. Game over");
                            game_over = true;
    
                            break;    
                        }       
                    }
                }                
            }
            System.out.println("Congratulations! You have cleared all three levels");
        }
    }
}
