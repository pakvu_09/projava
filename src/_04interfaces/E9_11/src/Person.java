public class Person {

    private String name;
    private int year_of_birth;

    public Person(String name, int year) {
        this.name = name;
        this.year_of_birth = year;
    }

    public String getName() {
        return name;
    }

    public int getBirthYear() {
        return year_of_birth;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBirthYear(int year) {
        this.year_of_birth = year;
    }

    @Override
    public String toString() {
        return "Person: " + name + "; Year of birth: " + year_of_birth;
    }
}
