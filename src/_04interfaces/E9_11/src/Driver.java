public class Driver {
    public static void main(String[] args) throws Exception {
        Person person = new Person("Trump", 2020); // add a person
        System.out.println(person.toString());

        Student student = new Student("Kelly", 2020, "Computer Science"); // add a student
        System.out.println(student.toString());

        Instructor instructor = new Instructor("PakVu", 2020, 140000); // add an instructor
        System.out.println(instructor.toString());
    }
}
