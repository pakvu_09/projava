public class WorldClock extends Clock {

    private int timeOffset;

    public WorldClock(int numZone) {
        timeOffset = numZone; // numer of zones ahead
    }

    public int getTimeOffset() {
        return timeOffset;
    }

    public void setTimeOffset(int timeDiff) {
        this.timeOffset = timeDiff;
    }

    @Override
    public String getHours() {
        int newHour = Integer.parseInt(super.getHours()) - timeOffset;
        if (newHour < 0) {
            newHour += 24; // make sure hour is always positive
        }
        return Integer.toString(newHour);
    }
}
