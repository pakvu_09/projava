public class Driver {
    public static void main(String[] args) throws Exception {
        // normal clock
        Clock clock = new Clock();
        System.out.println("Hour: " + clock.getHours());
        System.out.println("Minute: " + clock.getMinutes());
        System.out.println("Time: " + clock.getTime());

        // world clock with 3 time zones ahead, override get hours method
        WorldClock worldClock = new WorldClock(3);
        System.out.println("Hour: " + worldClock.getHours());
        System.out.println("Minute: " + worldClock.getMinutes());
        System.out.println("Time: " + worldClock.getTime());
    }
}
