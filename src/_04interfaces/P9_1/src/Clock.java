public class Clock {

    private String[] times;

    // declare constructor
    public Clock() {
        String time = java.time.LocalTime.now().toString(); // get time
        times = time.split(":"); // split in to hour, minute and second
    }

    // getter/ setter
    public String[] getTimes() {
        return times;
    }

    public void setTimes(String[] times) {
        this.times = times;
    }

    public String getHours() {
        return times[0];
    }

    public String getMinutes() {
        return times[1];
    }

    public String getTime() {
        return this.getHours() + ":" + this.getMinutes();
    }
}
