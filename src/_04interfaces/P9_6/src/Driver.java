import java.util.Scanner;

public class Driver {
    public static void main(String[] args) throws Exception {
        Appointment[] apt = new Appointment[5];
        apt[0] = new Daily("Java Programming", 2020, 9, 9);
        apt[1] = new Monthly("Car washing", 2020, 1, 1);
        apt[2] = new OneTime("Tax return", 2020, 4, 30);
        apt[3] = new Daily("100 push up, 100 pull up", 2020, 1, 1);
        apt[4] = new Daily("Drinking Starbucks expresso", 2020, 1, 1);

        Scanner in = new Scanner(System.in);
        System.out.print("Year: ");
        int year = in.nextInt();
        System.out.print("Month: ");
        int month = in.nextInt();
        System.out.print("Day: ");
        int day = in.nextInt();

        in.close();

        for (int i = 0; i < apt.length; i++) {
            if (apt[i].occursOn(year, month, day)) {
                System.out.println(apt[i].getDescription()); // check if there is an appointment
            }
        }
    }
}
