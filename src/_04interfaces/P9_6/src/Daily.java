public class Daily extends Appointment {
    public Daily(String description, int year, int month, int day) {
        super(description, year, month, day);
    }

    @Override
    public boolean occursOn(int year, int month, int day) {
        // everyday after the current day
        return (year == this.getYear() && month == this.getMonth() && day >= this.getDay())
                || (year == this.getYear() && month > this.getMonth()) || (year > this.getYear());
    }
}
