public class Monthly extends Appointment {
    public Monthly(String description, int year, int month, int day) {
        super(description, year, month, day);
    }

    @Override
    public boolean occursOn(int year, int month, int day) {
        // the same day of month after today
        return (year == this.getYear() && month >= this.getMonth() && day == this.getDay())
                || (year > this.getYear() && day == this.getDay());
    }

}
