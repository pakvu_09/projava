public class OneTime extends Appointment {
    public OneTime(String description, int year, int month, int day) {
        super(description, year, month, day);
    }

    @Override
    public boolean occursOn(int year, int month, int day) {
        // if the date is exactly the same
        return year == this.getYear() && month == this.getMonth() && day == this.getDay();
    }
}
