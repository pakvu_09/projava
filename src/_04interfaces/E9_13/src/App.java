public class App {
    public static void main(String[] args) {
        BetterRectangle rect = new BetterRectangle(5, 10); // set height and width
        System.out.println("The perimeter is: " + rect.getPerimeter());
        System.out.println("The area is: " + rect.getArea());
    }
}
