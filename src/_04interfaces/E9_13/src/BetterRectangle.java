import java.awt.*;

public class BetterRectangle extends Rectangle {

    public BetterRectangle(int x, int y) {
        super.setLocation(0, 0);
        super.setSize(x, y);
    }

    public int getPerimeter() {
        return 2 * (super.height + super.width);
    }

    public int getArea() {
        return super.height * super.width;
    }

}
