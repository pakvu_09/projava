#########################################################################
# Use this file to answer Review Exercises from the Big Java textbook
#########################################################################

#########################################################################
################################ R9.1 ###################################
#########################################################################

Superclas and subclass
    a.Employee (Superclass), Manager (Subclass)
    b.GraduateStudent (Subclass), Student (Superclass)
    c.Person (Superclass), Student (Subclass)
    d.Employee (Superclass), Professor (Subclass)
    e.BankAccount (Superclass), CheckingAccount (Subclass)
    f.Vehicle (Superclass), Car (Subclass)
    g.Vehicle (Superclass), Minivan (Subclass)
    h.Car (Superclass), Minivan (Subclass)
    i.Truck (Subclass), Vehicle (Superclass)

#########################################################################
################################ R9.2 ###################################
#########################################################################

It is not useful to have subclass small appliance because it is too broad.
Toaster, CarVacuum and TravelIron do not have many common traits except being small.

#########################################################################
################################ R9.4 ###################################
#########################################################################

SavingsAccount
    Inherit: deposit() and getBalance()
    Override: withdraw() and monthEnd()
    Add: setInterestRate()


#########################################################################
################################ R9.6 ###################################
#########################################################################

Sandwich
    a.Legal
    b.Illegal
    c.Illegal
    d.Legal


#########################################################################
################################ R9.7 ###################################
#########################################################################

Inheretence -use the UML diagram tool in IntelliJ and indicate to the grader where uml file is located

    "_04interface/R9_7.jpeg"

    
#########################################################################
################################ R9.8 ###################################
#########################################################################


Inheretence -use the UML diagram tool in IntelliJ and indicate to the grader where uml file is located

    "_04interface/R9_8.jpeg"


#########################################################################
################################ R9.9 ###################################
#########################################################################

Inheretence -use the UML diagram tool in IntelliJ and indicate to the grader where uml file is located

    "_04interface/R9_9.jpeg"


#########################################################################
################################ R9.10 ##################################
#########################################################################

The (BankAccount) x cast is casting the variable type of the object without changing its value
whereas the (int) x cast can actually change the underlying primitive value.


#########################################################################
################################ R9.11 ##################################
#########################################################################

Instance of operator
    a. True
    b. True
    c. False
    d. True
    e. True
    f. False


#########################################################################
################################ R9.14 ##################################
#########################################################################

Edible interface
    a, c are legel, the rests are Illegal



