public class BasicAccount extends BankAccount {

    public BasicAccount(double initialBalance) {

        super(initialBalance);
    }

    @Override
    public void withdraw(double amount) {

        double balance = getBalance();
        if (balance < amount) {
            System.out.println("Do not have enough money. Withdraw denied!");

        } else {

            super.withdraw(amount);

        }

    }
}
