public class BankAccount {
    private double balance;

    /**
     * Constructs a bank account with a zero balance.
     */
    public BankAccount() {
        balance = 0;
    }

    /**
     * Constructs a bank account with a given balance.
     * 
     * @param initialBalance the initial balance
     */
    public BankAccount(double initialBalance) {
        balance = initialBalance;
    }

    /**
     * Deposits money into this account.
     * 
     * @param amount the amount to deposit
     */
    public void deposit(double amount) {
        balance = balance + amount;
    }

    /**
     * Makes a withdrawal from this account, or charges a penalty if sufficient
     * funds are not available.
     * 
     * @param amount the amount of the withdrawal
     */
    public void withdraw(double amount) {
        balance = balance - amount;
    }

    /**
     * Carries out the end of month processing that is appropriate for this account.
     */
    public void monthEnd() {
    }

    /**
     * Gets the current balance of this bank account.
     * 
     * @return the current balance
     */
    public double getBalance() {
        return balance;
    }
}
