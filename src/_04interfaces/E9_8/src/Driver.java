public class Driver {
    public static void main(String[] args) throws Exception {
        BasicAccount acc = new BasicAccount(10);
        System.out.println("Current balance: " + acc.getBalance());
        acc.withdraw(5); // withdraw 5 dollars, 5 dollars remaining
        System.out.println("Withdrawn 5 dollars. Current balance: " + acc.getBalance());

        System.out.println("Withdrawing 10 dollars");
        acc.withdraw(10); // withdraw 10 dollars, can not withdraw
        System.out.println("Current balance: " + acc.getBalance());
    }
}
