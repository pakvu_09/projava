public class SodaCan implements Measurable {

    private double height;
    private double radius;

    // Declare getter/ setter for height

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    // Declare getter/ setter for radius
    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public SodaCan(double height, double radius) {
        this.height = height;
        this.radius = radius;
    }

    // Compute surface area of a soda can
    public double getSurfaceArea() {
        return 2 * Math.PI * (radius * radius) + 2 * Math.PI * radius * height;
    }

    public double getVolume() {
        return Math.PI * (radius * radius) * height;
    }

    public double getMeasure() {
        return getSurfaceArea();

    }
}
