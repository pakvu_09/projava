public class Driver {
    public static void main(String[] args) throws Exception {

        // Declare an array of three cans
        Measurable[] cans = new Measurable[3];
        SodaCan can_1 = new SodaCan(2, 3);
        SodaCan can_2 = new SodaCan(5, 6);
        SodaCan can_3 = new SodaCan(1, 4);

        cans[0] = can_1;
        cans[1] = can_2;
        cans[2] = can_3;

        System.out.println("The surface area of these three cans are: " + can_1.getSurfaceArea() + " "
                + can_2.getSurfaceArea() + " " + can_3.getSurfaceArea());

        System.out.println("Average surface area is : " + average(cans)); // compute the average surface area
    }

    public static double average(Measurable[] objects) {
        if (objects.length == 0) {
            return 0;
        }
        double sum = 0;
        for (Measurable obj : objects) {
            double measure = obj.getMeasure();
            sum = sum + measure;
        }
        double result = sum / objects.length;
        return result;
    }
}
