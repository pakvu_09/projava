// Read all words from a file into an ArrayList<String>, then turn it into a parallel
// stream. Use the dictionary file words.txt provided with the book’s companion code.
// Use filters and the findAny method to find any palindrome that has at least five
// letters, then print the word. What happens when you run the program multiple
// times?

// Use this txt version of War and Peace -> https://archive.org/stream/warandpeace030164mbp/warandpeace030164mbp_djvu.txt


// When running the program multiple times there is a 256 code error

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class E19_14 {
    public static void main(String[] args) throws IOException {
        String filename = "C:/Bach/Master_Uni_of_Chicago/Fall2020/Java/projava/src/_07streams/war_and_peace.txt";
        List<String> words = new ArrayList<>();
        try(Stream<String> lines = Files.lines(Paths.get(filename))) {
            lines.forEach(w -> {

                // create a list of words from each line
                String[] words_one_line = w.split(" ");

                // combine words from each line into total list of words of the whole text
                words.addAll(Arrays.asList(words_one_line));
            });

            Stream<String> words_stream = words.stream();
            String result = words_stream
                    .parallel()
                    .filter(w -> w.length()>=5)
                    .filter(w -> isPalindrome(w))
                    .findAny()
                    .orElse("Not Found");
            System.out.println("Palindrome found: " + result);
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    // Declare a helper function for recursive method
    public static boolean isPalindrome(String word){

        // Declare a base case
        if(word.length() == 0 || word.length() == 1){
            return true;
        }

        // recursively check if a word is palindrome
        else if(word.substring(0,1).equals(word.substring(word.length()-1))){
            return isPalindrome(word.substring(1,word.length()-1));
        }
        return false;
    }
}

