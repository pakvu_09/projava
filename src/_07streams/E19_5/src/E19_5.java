// Write a method
// public static <T> String toString(Stream<T> stream, int n)
// that turns a Stream<T> into a comma-separated list of its first n elements.


import java.util.stream.Collectors;
import java.util.stream.Stream;

public class E19_5 {
    public static void main(String[] args) {

        String[] names = {"Bread", "Peanut", "Butter", "Yam", "Olive oil", "Coffee"};
        Stream<String> stream = Stream.of(names);

        // Print a comma-separated list of its first 3 elements
        System.out.println(toString(stream, 3));
   
    }

    public static <T> String toString(Stream<T> stream, int n){
        String result = stream.map(w -> w.toString()).limit(n).collect(Collectors.joining(", "));
        return result;
    }

}
