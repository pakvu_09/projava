import java.util.Scanner;

public class YodaSpeakRecursive {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        // The force is strong with you
        System.out.print("Enter a sentence: ");
        String sentence = input.nextLine();
        String[] words = sentence.split(" ");

        // Implement this program recursively.
        RecursiveHelper(words,words.length-1);

        input.close();
    }

    // implement a helper function for recursive method
    public static void RecursiveHelper(String[] words, int index){
        if(index == 0){
            System.out.print(words[index]);
        }
        else{
            System.out.print(words[index]+" ");
            RecursiveHelper(words, index-1);
        }
    }
}
