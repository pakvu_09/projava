/*
Given an integer price, list all possible ways of paying for it with $100, $20, $5, and $1
bills, using recursion. Don’t list duplicates..
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class E13_20 {
    public static void main(String[] args) {
        List<Integer> availBills = new ArrayList<Integer>(4); //available bills
        availBills.add(100);
        availBills.add(20);
        availBills.add(5);
        availBills.add(1);

        Scanner input = new Scanner(System.in);
        System.out.print("Enter an integer number: ");
        int num = input.nextInt();

        System.out.println("All the payment combination of " + num + " $ is: ");
        payBill(num, "", availBills);

        input.close();

    }

    public static void payBill(int num, String currBills, List<Integer> avail){
        if(num == 0){
            // base case, there is no money to pay
            System.out.println(currBills);
        }
        for(int i = 0; i < avail.size(); i ++){
            if(num >= avail.get(i)){
                // if the number is larger than bill, we can pay using that bill
                String nextBills =  currBills + " $" + avail.get(i) + " "; // make string
                payBill(num - avail.get(i),nextBills , avail.subList(i,avail.size()));
            }
        }



    }
}