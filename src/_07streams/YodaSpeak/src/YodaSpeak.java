import java.util.Scanner;

public class YodaSpeak {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        // The force is strong with you
        System.out.print("Enter a sentence: ");
        String sentence = input.nextLine();
        String[] words = sentence.split(" ");

        // Implement this program iteratively.
        for (int i = words.length - 1; i >= 0 ; i--) {
            System.out.print(words[i]+" ");
        }


        input.close();
    }
}