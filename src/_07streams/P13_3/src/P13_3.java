// With a longer number, you may need more than one word to remember it on a phone
// pad. For example, 263-346-5282 is CODE IN JAVA. Using your work from Exercise ••
// P13.2, write a program that, given any number, lists all word sequences that spell the
// number on a phone pad.

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;


public class P13_3 {
    public static void main(String[] args) throws IOException {

        // Create a 2D matrix of characters

        String[][] letters = {{"0"},
                {"1"},
                {"A", "B", "C"},
                {"D", "E", "F"},
                {"G", "H", "I"},
                {"J", "K", "L"},
                {"M", "N", "O"},
                {"P", "Q", "R", "S"},
                {"T", "U", "V"},
                {"W", "X", "Y", "Z"}};

        Scanner in = new Scanner(System.in);

        // Load all words in txt file into a dictionary for constant time look up
        String dictFile = "C:/Bach/Master_Uni_of_Chicago/Fall2020/Java/projava/src/_07streams/words.txt";
        Stream<String> words = Files.lines(Paths.get(dictFile));
        Set<String> dict = new HashSet<>();
        words.parallel().forEach(w->{
            if(w.length()>1){
                dict.add(w.toUpperCase());
            }
        });

        System.out.print("Enter the number: ");
        int num = in.nextInt();
        System.out.println("All the actual words from a give number are: ");

        
        Set<String> empty_dict = new HashSet<>();
        recursive_num_convert(Integer.toString(num), letters, "", dict, empty_dict);

        in.close();
    }

    //helper function to get all unique combination
    private static void recursive_num_convert(String num, String[][] letters, String word, Set<String> dict, Set<String> unique_dict) {

        
        // Declare base case
        if (num.length() == 0)

            if ((isWord(word, dict)) && (!unique_dict.contains(word)))

            // The reason why we need a unique dict is to avoid duplicate a representation twice
                System.out.println(word);
                unique_dict.add(word);
        
        
        for (int i = 0; i < num.length(); i++) {

            int index = Integer.parseInt(num.substring(i, i+1));
            String[] letterList = letters[index];

            for (int j = 0; j < letterList.length; j++) {
                String res = word + letterList[j];
                
                recursive_num_convert(num.substring(1), letters, res, dict,unique_dict );

            }
                
        }

        


    }
    

    //check if word exists
    private static boolean isWord(String spelling, Set<String> dict){
        if (dict.contains(spelling)){
            return true;
        }
        return false;
    }



}