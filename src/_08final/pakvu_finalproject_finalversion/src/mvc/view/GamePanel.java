package mvc.view;

import mvc.controller.Game;
import mvc.model.CommandCenter;
import mvc.model.Falcon;
import mvc.model.Movable;

import java.awt.*;
import java.util.ArrayList;
import java.awt.image.BufferedImage;
import java.io.File;


import javax.imageio.ImageIO;



public class GamePanel extends Panel {
	
	// ==============================================================
	// FIELDS 
	// ============================================================== 
	 
	// The following "off" vars are used for the off-screen double-bufferred image. 
	private Dimension dimOff;
	private Image imgOff;
	private Graphics grpOff;
	
	private GameFrame gmf;
	private Font fnt = new Font("SansSerif", Font.BOLD, 12);
	private Font fntBig = new Font("SansSerif", Font.BOLD + Font.ITALIC, 36);
	private FontMetrics fmt; 
	private int nFontWidth;
	private int nFontHeight;
	private String strDisplay = "";
	

	// ==============================================================
	// CONSTRUCTOR 
	// ==============================================================
	
	public GamePanel(Dimension dim){
	    gmf = new GameFrame();
		gmf.getContentPane().add(this);
		gmf.pack();
		initView();
		
		gmf.setSize(dim);
		gmf.setTitle("Asteroid_MPCS_PakVu");
		gmf.setResizable(false);
		gmf.setVisible(true);
		this.setFocusable(true);
	}
	
	
	// ==============================================================
	// SUPPLEMENTAL METHODS 
	// ==============================================================
	
	private void drawScore(Graphics g) {
		g.setColor(Color.white);
		g.setFont(fnt);
		if (CommandCenter.getInstance().getScore() != 0) {
			g.drawString("SCORE :  " + CommandCenter.getInstance().getScore(), nFontWidth, nFontHeight);
		} else {
			g.drawString("NO SCORE", nFontWidth, nFontHeight);
		}
	}

	private void drawBackGround(Graphics g) {

		BufferedImage image;

		try {
			image = ImageIO.read(new File("./src/images/background.png"));
			g.setColor(Color.white);
			g.drawImage(image, 0, 0, Game.DIM.width, Game.DIM.height, null);

		} catch (Exception e) {
            e.printStackTrace();
        }		

	}

	private void drawHeader(Graphics g) {

		BufferedImage image;

		try {
			image = ImageIO.read(new File("./src/images/asteroids-header.jpg"));
			g.setColor(Color.white);
			g.drawImage(image, 50, 50, Game.DIM.width - 100, Game.DIM.height/4, null);

		} catch (Exception e) {
            e.printStackTrace();
        }		

	}

	private void drawLevelIndicator(Graphics g) {

		if (CommandCenter.getInstance().isPlaying()) {
			g.setColor(Color.white);
			g.setFont(fnt);
			g.drawString("LEVEL :  " + CommandCenter.getInstance().getLevel(), 6 * nFontWidth, nFontHeight);
		} else {
			g.setColor(Color.white);
			g.setFont(fnt);
			g.drawString("LEVEL :  ", 6 * nFontWidth, nFontHeight);
		}

	}

	private void drawElapsedTime(Graphics g) {
		if (CommandCenter.getInstance().isPlaying()) {

			if (CommandCenter.getInstance().isPaused()) {
				CommandCenter.getInstance().resetLevelEpochTime();
			}
			g.setColor(Color.white);
			g.setFont(fnt);
			g.drawString("Elapsed Time :  " + CommandCenter.getInstance().getElapsedTime() / 1000  / 60 + " : " +
												CommandCenter.getInstance().getElapsedTime() / 1000 % 60 + " : " +
												CommandCenter.getInstance().getElapsedTime() % 100,
												10 * nFontWidth, nFontHeight);
		} else {
			g.setColor(Color.white);
			g.setFont(fnt);
			g.drawString("Elapsed Time :  " + "00 : 00 : 00", 10 * nFontWidth, nFontHeight);
		}

	}
	
	private void drawFalconHealth(Graphics g) {

		Falcon fal = CommandCenter.getInstance().getFalcon();

		if (fal != null){
			g.drawString("Falcon Health: ", 10, 35);
			g.setColor(Color.gray);
			g.fillRect(5, 40, 100, 25);
			g.setColor(Color.green);			
			g.fillRect(5, 40, fal.getFalconHealth() * 20, 25);	
			g.setColor(Color.white);
			g.drawRect(5, 40, 100, 25);
		}

	}

	private void drawLifeRemained(Graphics g) {
		g.setColor(Color.white);
		g.setFont(fnt);
		if (CommandCenter.getInstance().isPlaying()) {
			g.drawString("Lives: " + CommandCenter.getInstance().getNumFalcons(), 19 * nFontWidth, nFontHeight);
		} else {
			g.drawString("Lives: 3", 19 * nFontWidth, nFontHeight);

		}
	}

	private void drawShieldsRemained(Graphics g) {
		g.setColor(Color.white);
		g.setFont(fnt);
		if (CommandCenter.getInstance().isPlaying()) {
			g.drawString("Shields: " + CommandCenter.getInstance().getNumShields(), 23 * nFontWidth, nFontHeight);
		} else {
			g.drawString("Shields: 3", 23 * nFontWidth, nFontHeight);

		}
	}
	
	@SuppressWarnings("unchecked")
	public void update(Graphics g) {
		if (grpOff == null || Game.DIM.width != dimOff.width
				|| Game.DIM.height != dimOff.height) {
			dimOff = Game.DIM;
			imgOff = createImage(Game.DIM.width, Game.DIM.height);
			grpOff = imgOff.getGraphics();
		}
		// Fill in background with black.
		grpOff.setColor(Color.black);
		grpOff.fillRect(0, 0, Game.DIM.width, Game.DIM.height);

		drawBackGround(grpOff);
		drawScore(grpOff);
		drawLevelIndicator(grpOff);
		drawElapsedTime(grpOff);
		drawShieldsRemained(grpOff);
		drawLifeRemained(grpOff);
		drawFalconHealth(grpOff);
		
		if (!CommandCenter.getInstance().isPlaying()) {
			displayTextOnScreen();
		} else if (CommandCenter.getInstance().isPaused()) {
			strDisplay = "Game Paused";
			grpOff.drawString(strDisplay,
					(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4);

			strDisplay = "Use the arrow keys to turn and thrust";
			grpOff.drawString(strDisplay,
					(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
							+ nFontHeight + 40);
	
			strDisplay = "Use the space bar to fire";
			grpOff.drawString(strDisplay,
					(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
							+ nFontHeight + 70);
	
			strDisplay = "Press 'S' to Start";
			grpOff.drawString(strDisplay,
					(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
							+ nFontHeight + 100);
	
			strDisplay = "Press 'P' to Pause";
			grpOff.drawString(strDisplay,
					(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
							+ nFontHeight + 130);
	
			strDisplay = "Press 'Q' to Quit";
			grpOff.drawString(strDisplay,
					(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
							+ nFontHeight + 160);
	
			strDisplay = "Left pinkie on 'A' for Shield";
			grpOff.drawString(strDisplay,
					(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
							+ nFontHeight + 190);
	
			strDisplay = "Left index finger on 'F' for Normal Missile";
			grpOff.drawString(strDisplay,
					(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
							+ nFontHeight + 220);
	
			strDisplay = "Press and hold E for Guided Missile";
			grpOff.drawString(strDisplay,
					(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
							+ nFontHeight + 250);
	
			strDisplay = "Press D for Hyperspace";
			grpOff.drawString(strDisplay,
					(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
							+ nFontHeight + 280);


			strDisplay = "Press M to mute background music";
			grpOff.drawString(strDisplay,
					(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
							+ nFontHeight + 310);
		}
		
		//playing and not paused!
		else {
			
			//draw them in decreasing level of importance
			//friends will be on top layer and debris on the bottom
			iterateMovables(grpOff,
					(ArrayList<Movable>)  CommandCenter.getInstance().getMovFriends(),
					(ArrayList<Movable>)  CommandCenter.getInstance().getMovFoes(),
					(ArrayList<Movable>)  CommandCenter.getInstance().getMovFloaters(),
					(ArrayList<Movable>)  CommandCenter.getInstance().getMovDebris());


			drawNumberShipsLeft(grpOff);
			if (CommandCenter.getInstance().isGameOver()) {
				CommandCenter.getInstance().setPlaying(false);
				//bPlaying = false;
			}
		}
		//draw the double-Buffered Image to the graphics context of the panel
		g.drawImage(imgOff, 0, 0, this);
	} 


	
	//for each movable array, process it.
	private void iterateMovables(Graphics g, ArrayList<Movable>...movMovz){
		
		for (ArrayList<Movable> movMovs : movMovz) {
			for (Movable mov : movMovs) {

				mov.move();
				mov.draw(g);

			}
		}
		
	}
	

	// Draw the number of falcons left on the bottom-right of the screen. 
	private void drawNumberShipsLeft(Graphics g) {
		Falcon fal = CommandCenter.getInstance().getFalcon();
		double[] dLens = fal.getLengths();
		int nLen = fal.getDegrees().length;
		Point[] pntMs = new Point[nLen];
		int[] nXs = new int[nLen];
		int[] nYs = new int[nLen];
	
		//convert to cartesean points
		for (int nC = 0; nC < nLen; nC++) {
			pntMs[nC] = new Point((int) (10 * dLens[nC] * Math.sin(Math
					.toRadians(90) + fal.getDegrees()[nC])),
					(int) (10 * dLens[nC] * Math.cos(Math.toRadians(90)
							+ fal.getDegrees()[nC])));
		}
		
		//set the color to white
		g.setColor(Color.white);
		//for each falcon left (not including the one that is playing)
		for (int nD = 1; nD < CommandCenter.getInstance().getNumFalcons(); nD++) {
			//create x and y values for the objects to the bottom right using cartesean points again
			for (int nC = 0; nC < fal.getDegrees().length; nC++) {
				nXs[nC] = pntMs[nC].x + Game.DIM.width - (20 * nD);
				nYs[nC] = pntMs[nC].y + Game.DIM.height - 40;
			}
			g.drawPolygon(nXs, nYs, nLen);
		} 
	}
	
	private void initView() {
		Graphics g = getGraphics();			// get the graphics context for the panel
		g.setFont(fnt);						// take care of some simple font stuff
		fmt = g.getFontMetrics();
		nFontWidth = fmt.getMaxAdvance();
		nFontHeight = fmt.getHeight();
		g.setFont(fntBig);					// set font info
	}
	
	// This method draws some text to the middle of the screen before/after a game
	private void displayTextOnScreen() {


		if (!CommandCenter.getInstance().getGameInitiated()) {

			if (!CommandCenter.getInstance().isGamePlot()) {

				drawHeader(grpOff);

				strDisplay = "PRESS 'S' TO START";
				grpOff.setFont(fnt);
				grpOff.drawString(strDisplay,
						(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
								+ nFontHeight + 100);

				strDisplay = "PRESS 'Q' TO QUIT";
				grpOff.drawString(strDisplay,
						(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
								+ nFontHeight + 130);
	
				strDisplay = "PRESS 'P' TO PAUSE & INSTRUCTION";
				grpOff.drawString(strDisplay,
						(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
								+ nFontHeight + 160);

				// strDisplay = "PRESS 'ENTER' FOR GAME PLAY ";
				// grpOff.drawString(strDisplay,
				// 		(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
				// 				+ nFontHeight + 190);
	



			} else {

				strDisplay = "In this game you will control a millenium falcon to destroy all the asteroids and enemies drones.";
				grpOff.drawString(strDisplay,
						(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4 - 60);

				strDisplay = "You have 3 falcons and 3 shields to start with, get health floater to increase your falcon number.";
						grpOff.drawString(strDisplay,
								(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4 - 40);

				strDisplay = "Each falcon can withstand up to 5 assaults. Falcon Health is demonstrated on top left corner.";
						grpOff.drawString(strDisplay,
								(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4 - 20);	
								
				strDisplay = "You will have 2 minutes to get as many points as possible. Points can be gained from destroying asteroids and enemies drones.";
						grpOff.drawString(strDisplay,
								(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4 );
	
				strDisplay = "Use the arrow keys to turn and thrust";
				grpOff.drawString(strDisplay,
						(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
								+ nFontHeight + 40);
		
				strDisplay = "Use the space bar to fire";
				grpOff.drawString(strDisplay,
						(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
								+ nFontHeight + 70);
		
				strDisplay = "Press 'S' to Start";
				grpOff.drawString(strDisplay,
						(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
								+ nFontHeight + 100);
		
				strDisplay = "Press 'P' to Pause";
				grpOff.drawString(strDisplay,
						(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
								+ nFontHeight + 130);
		
				strDisplay = "Press 'Q' to Quit";
				grpOff.drawString(strDisplay,
						(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
								+ nFontHeight + 160);
		
				strDisplay = "Left pinkie on 'A' for Shield";
				grpOff.drawString(strDisplay,
						(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
								+ nFontHeight + 190);
		
				strDisplay = "Left index finger on 'F' for Normal Missile";
				grpOff.drawString(strDisplay,
						(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
								+ nFontHeight + 220);
		
				strDisplay = "Press and hold E for Guided Missile";
				grpOff.drawString(strDisplay,
						(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
								+ nFontHeight + 250);
		
				strDisplay = "Press D for Hyperspace";
				grpOff.drawString(strDisplay,
						(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
								+ nFontHeight + 280);
	
	
				strDisplay = "Press M to mute background music";
				grpOff.drawString(strDisplay,
						(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
								+ nFontHeight + 310);
			}


		} else if (CommandCenter.getInstance().getTimeUp()) {

			if (CommandCenter.getInstance().getTimeUp()) {
				strDisplay = "Time is UP. Your final score is: " + CommandCenter.getInstance().getScore()+  " !";

				grpOff.drawString(strDisplay,
					(Game.DIM.width - fmt.stringWidth(strDisplay)) / 2, Game.DIM.height / 4
							+ nFontHeight + 130);
			}

		}



	}
	
	public GameFrame getFrm() {return this.gmf;}
	public void setFrm(GameFrame frm) {this.gmf = frm;}	
}