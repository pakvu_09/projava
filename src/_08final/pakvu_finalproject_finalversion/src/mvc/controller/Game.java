package mvc.controller;


import mvc.model.*;
import mvc.view.GamePanel;
import sounds.Sound;

import javax.sound.sampled.Clip;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

// ===============================================
// == This Game class is the CONTROLLER
// ===============================================

public class Game implements Runnable, KeyListener {

	// ===============================================
	// FIELDS
	// ===============================================

	public static final Dimension DIM = new Dimension(1100, 900); //the dimension of the game.
	private GamePanel gmpPanel;
	public static Random R = new Random();
	public final static int ANI_DELAY = 45; // milliseconds between screen
											// updates (animation)
	private Thread thrAnim;
	private int nLevel = 1;
	private int nTick = 0;
	private long score = 0; // to implement scoring

	private boolean bMuted = true;
	

	private final int PAUSE = 80, // p key
			QUIT = 81, // q key
			LEFT = 37, // rotate left; left arrow
			RIGHT = 39, // rotate right; right arrow
			UP = 38, // thrust; up arrow
			START = 83, // s key
			FIRE = 32, // space key
			MUTE = 77, // m-key mute
			SHIELD = 65, // a-key
			HYPER = 68,	// d key
			ENTER = 10,
			MISSILE = 70, // fire FALCON MISSILE
			SPECIAL = 69; 	// fire guided missle;  E key

	// for possible future use

	// NUM_ENTER = 10, 				// hyp


	private Clip clpThrust;
	private Clip clpMusicBackground;

	private static final int SPAWN_NEW_SHIP_FLOATER = 1200;

	// An enum can just like a class, have attributes and methods
	public enum STATUS{
		MENU,
		GAME,
		HELP,
		GAME_OVER
	};

	public static STATUS status = STATUS.MENU;



	// ===============================================
	// ==CONSTRUCTOR
	// ===============================================

	public Game() {

		gmpPanel = new GamePanel(DIM);
		gmpPanel.addKeyListener(this);
		clpThrust = Sound.clipForLoopFactory("whitenoise.wav");
		clpMusicBackground = Sound.clipForLoopFactory("music-background.wav");
		

	}

	// ===============================================
	// ==METHODS
	// ===============================================

	public static void main(String args[]) {
		EventQueue.invokeLater(new Runnable() { // uses the Event dispatch thread from Java 5 (refactored)
					public void run() {
						try {
							Game game = new Game(); // construct itself
							game.fireUpAnimThread();

						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
	}

	private void fireUpAnimThread() { // called initially
		if (thrAnim == null) {
			thrAnim = new Thread(this); // pass the thread a runnable object (this)
			thrAnim.start();
		}
	}

	// implements runnable - must have run method
	public void run() {

		// lower this thread's priority; let the "main" aka 'Event Dispatch'
		// thread do what it needs to do first
		thrAnim.setPriority(Thread.MIN_PRIORITY);

		// and get the current time
		long lStartTime = System.currentTimeMillis();

		// this thread animates the scene
		while (Thread.currentThread() == thrAnim) {
			tick();
			spawnNewShipFloater();
			gmpPanel.update(gmpPanel.getGraphics()); // update takes the graphics context we must 
														// surround the sleep() in a try/catch block
														// this simply controls delay time between 
														// the frames of the animation

			//this might be a good place to check for collisions
			checkCollisions();
			//this might be a good place to check if the level is clear (no more foes)
			//if the level is clear then spawn some big asteroids -- the number of asteroids 
			//should increase with the level. 
			// Also spawn enemy drone in this step

			checkNewLevel();
			checkTimeUp();

			try {
				// The total amount of time is guaranteed to be at least ANI_DELAY long.  If processing (update) 
				// between frames takes longer than ANI_DELAY, then the difference between lStartTime - 
				// System.currentTimeMillis() will be negative, then zero will be the sleep time
				lStartTime += ANI_DELAY;
				Thread.sleep(Math.max(0,
						lStartTime - System.currentTimeMillis()));
			} catch (InterruptedException e) {
				// just skip this frame -- no big deal
				continue;
			}
		} // end while
	} // end run

	private void checkCollisions() {

		Point pntFriendCenter, pntFoeCenter;
		int nFriendRadiux, nFoeRadiux;

		for (Movable movFriend : CommandCenter.getInstance().getMovFriends()) {
			for (Movable movFoe : CommandCenter.getInstance().getMovFoes()) {

				pntFriendCenter = movFriend.getCenter();
				pntFoeCenter = movFoe.getCenter();
				nFriendRadiux = movFriend.getRadius();
				nFoeRadiux = movFoe.getRadius();

				//detect collision
				if (pntFriendCenter.distance(pntFoeCenter) < (nFriendRadiux + nFoeRadiux)) {

					//falcon
					if ((movFriend instanceof Falcon) ){
						// Falcon has an automative protection for about 40 frames since the start of each level
						if (!CommandCenter.getInstance().getFalcon().getProtected() & !CommandCenter.getInstance().bShieldOn()){

							if (((Falcon) movFriend).getFalconHealth() <= 1) {

								for (int deg = 0; deg < 360; deg += 20) {
									CommandCenter.getInstance().getOpsList().enqueue(new Debris(CommandCenter.getInstance().getFalcon(), deg), CollisionOp.Operation.ADD);
									
								}
								CommandCenter.getInstance().getOpsList().enqueue(movFriend, CollisionOp.Operation.REMOVE);
								CommandCenter.getInstance().spawnFalcon(false);
								
							}
							((Falcon) movFriend).decreaseFalconHealth();  //decrease falcon health by 1 for each collision

						}
					// falcon shield	
					} else if ((movFriend instanceof FalconShield)) {
                        if (((FalconShield) movFriend).getShieldHealth() <= 1) {
                            CommandCenter.getInstance().getOpsList().enqueue(movFriend, CollisionOp.Operation.REMOVE);
                            CommandCenter.getInstance().setShieldOn(false);
                        }
                        ((FalconShield) movFriend).decreaseShieldHealth();  //decrease shield health by 1 for each collision
                    }
					//not the falcon nor shield: any other weapons
					else {
						CommandCenter.getInstance().getOpsList().enqueue(movFriend, CollisionOp.Operation.REMOVE);
					}//end else

					//kill the foe and if asteroid, then spawn new asteroids
					//if the foe is EnemyDrone, only kill the drone if its health = 0.
					// The method killFoe has been modified to handleFoes reflect these changes

					handleFoes(movFoe, movFriend);
					// Sound.playSound("kapow.wav");

				}//end if 
			}//end inner for
		}//end outer for


		//check for collisions between falcon and floaters
		if (CommandCenter.getInstance().getFalcon() != null){
			Point pntFalCenter = CommandCenter.getInstance().getFalcon().getCenter();
			int nFalRadiux = CommandCenter.getInstance().getFalcon().getRadius();
			Point pntFloaterCenter;
			int nFloaterRadiux;
			
			for (Movable movFloater : CommandCenter.getInstance().getMovFloaters()) {
				pntFloaterCenter = movFloater.getCenter();
				nFloaterRadiux = movFloater.getRadius();
	
				//detect collision
				if (pntFalCenter.distance(pntFloaterCenter) < (nFalRadiux + nFloaterRadiux)) {
					
					// When the falcon collides with a NewShipFloater, increase the number of falcons by 1. 
					CommandCenter.getInstance().setNumFalcons(CommandCenter.getInstance().getNumFalcons() + 1);
					CommandCenter.getInstance().getOpsList().enqueue(movFloater, CollisionOp.Operation.REMOVE);
					Sound.playSound("pacman_eatghost.wav");
	
				}//end if 
			}//end inner for
		}//end if not null
		


		//we are dequeuing the opsList and performing operations in serial to avoid mutating the movable arraylists while iterating them above
		while(!CommandCenter.getInstance().getOpsList().isEmpty()){
			CollisionOp cop =  CommandCenter.getInstance().getOpsList().dequeue();
			Movable mov = cop.getMovable();
			CollisionOp.Operation operation = cop.getOperation();

			switch (mov.getTeam()){
				case FOE:
					if (operation == CollisionOp.Operation.ADD){
						CommandCenter.getInstance().getMovFoes().add(mov);
					} else {
						CommandCenter.getInstance().getMovFoes().remove(mov);
					}

					break;
				case FRIEND:
					if (operation == CollisionOp.Operation.ADD){
						CommandCenter.getInstance().getMovFriends().add(mov);
					} else {
						CommandCenter.getInstance().getMovFriends().remove(mov);
					}
					break;

				case FLOATER:
					if (operation == CollisionOp.Operation.ADD){
						CommandCenter.getInstance().getMovFloaters().add(mov);
					} else {
						CommandCenter.getInstance().getMovFloaters().remove(mov);
					}
					break;

				case DEBRIS:
					if (operation == CollisionOp.Operation.ADD){
						CommandCenter.getInstance().getMovDebris().add(mov);
					} else {
						CommandCenter.getInstance().getMovDebris().remove(mov);
					}
					break;


			}

		}
		//a request to the JVM is made every frame to garbage collect, however, the JVM will choose when and how to do this
		System.gc();
		
	}//end meth

	// The reason we need the movFriend in the new method is because different Falcon weapon
	// will have different effect on a foe.

	private void handleFoes(Movable movFoe, Movable movFriend) {

		Sound.playSound("kapow.wav");
		
		if (movFoe instanceof Asteroid){

			//we know this is an Asteroid, so we can cast without threat of ClassCastException
			Asteroid astExploded = (Asteroid)movFoe;
			//big asteroid 
			if(astExploded.getSize() == 0){
				//spawn two medium Asteroids
				CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(astExploded), CollisionOp.Operation.ADD);
				CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(astExploded), CollisionOp.Operation.ADD);
				//remove the original Foe
				CommandCenter.getInstance().getOpsList().enqueue(movFoe, CollisionOp.Operation.REMOVE);

			} 
			//medium size asteroid exploded
			else if(astExploded.getSize() == 1){
				//spawn three small Asteroids
				CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(astExploded), CollisionOp.Operation.ADD);
				CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(astExploded), CollisionOp.Operation.ADD);
				CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(astExploded), CollisionOp.Operation.ADD);
				//remove the original Foe
				CommandCenter.getInstance().getOpsList().enqueue(movFoe, CollisionOp.Operation.REMOVE);			
			// small size asteroid exploded
			} else {
				for (int deg = 0; deg < 360; deg += 20) {
					CommandCenter.getInstance().getOpsList().enqueue(new Debris(astExploded, deg), CollisionOp.Operation.ADD);
				}	

				//remove the original Foe
				CommandCenter.getInstance().getOpsList().enqueue(movFoe, CollisionOp.Operation.REMOVE);			
			}	

		} else if (movFoe instanceof EnemyDrone) {
			
			// Missile has more impact to Enemy drone than normal bullet
			if (movFriend instanceof FalconMissile) {
				((EnemyDrone) movFoe).setDroneHealth(((EnemyDrone) movFoe).getDroneHealth() - 3);
				Sound.playSound("droneHit.wav");
			} else {
				((EnemyDrone) movFoe).setDroneHealth(((EnemyDrone) movFoe).getDroneHealth() - 1);
				Sound.playSound("droneHit.wav");
			}
			//destroy the enemy drone once its health drops below 1
			if (((EnemyDrone) movFoe).getDroneHealth() < 1) {

				Sound.playSound("droneDeath.wav");
				for (int deg = 0; deg < 360; deg += 20) {
					CommandCenter.getInstance().getOpsList().enqueue(new Debris((EnemyDrone)movFoe, deg), CollisionOp.Operation.ADD);
				}	
				
				//remove the original Foe
				CommandCenter.getInstance().getOpsList().enqueue(movFoe, CollisionOp.Operation.REMOVE);

			}
		} else if (movFoe instanceof TitaniumAsteroid) {
			
			// Missile has more impact to Titanium Asteroid than normal bullet
			if (movFriend instanceof FalconMissile) {
				((TitaniumAsteroid) movFoe).setTAHealth(((TitaniumAsteroid) movFoe).getTAHealth() - 3);
				Sound.playSound("droneHit.wav");
			} else {
				((TitaniumAsteroid) movFoe).setTAHealth(((TitaniumAsteroid) movFoe).getTAHealth() - 1);
				Sound.playSound("droneHit.wav");
			}
			//destroy the enemy drone once its health drops below 1
			if (((TitaniumAsteroid) movFoe).getTAHealth() < 1) {

				Sound.playSound("droneDeath.wav");
				for (int deg = 0; deg < 360; deg += 20) {
					CommandCenter.getInstance().getOpsList().enqueue(new Debris((TitaniumAsteroid)movFoe, deg), CollisionOp.Operation.ADD);
				}	
				
				//remove the original Foe
				CommandCenter.getInstance().getOpsList().enqueue(movFoe, CollisionOp.Operation.REMOVE);

			}
		} else if (movFoe instanceof DroneWeapon){
			if (movFriend instanceof FalconShield || movFriend instanceof Falcon ) {

				CommandCenter.getInstance().getOpsList().enqueue(movFoe, CollisionOp.Operation.REMOVE);
			}
		}


		if (movFoe instanceof ScoreGenerator) {
			CommandCenter.getInstance().setScore(CommandCenter.getInstance().getScore() + ((ScoreGenerator) movFoe).getScore());
		}



	}

	//some methods for timing events in the game,
	//such as the appearance of UFOs, floaters (power-ups), etc. 
	public void tick() {
		if (nTick == Integer.MAX_VALUE)
			nTick = 0;
		else
			nTick++;
	}

	public int getTick() {
		return nTick;
	}

	private void spawnNewShipFloater() {
		//make the appearance of power-up dependent upon ticks and levels
		//the higher the level the more frequent the appearance
		if (nTick % (SPAWN_NEW_SHIP_FLOATER - nLevel * 7) == 0) {
			//CommandCenter.getInstance().getMovFloaters().enqueue(new NewShipFloater());
			CommandCenter.getInstance().getOpsList().enqueue(new NewShipFloater(), CollisionOp.Operation.ADD);
		}
	}

	// Called when user presses 's'
	private void startGame() {
		CommandCenter.getInstance().clearAll();
		CommandCenter.getInstance().initGame();
		// CommandCenter.getInstance().setLevel(0);
		CommandCenter.getInstance().setPlaying(true);
		CommandCenter.getInstance().setPaused(false);
		if (!bMuted)
		   clpMusicBackground.loop(Clip.LOOP_CONTINUOUSLY);
	}

	//this method spawns new asteroids
	private void spawnAsteroids(int nNum) {
		for (int nC = 0; nC < nNum; nC++) {
			
			//Asteroids with size of zero are big
			CommandCenter.getInstance().getOpsList().enqueue(new Asteroid(0), CollisionOp.Operation.ADD);

		}
	}

	//this method spawns new enemies drones
	private void spawnEnemyDrone(int nNum) {
		for (int nC = 0; nC < nNum; nC++) {

			
			CommandCenter.getInstance().getOpsList().enqueue(new EnemyDrone(CommandCenter.getInstance().getFalcon()), CollisionOp.Operation.ADD);
			Sound.playSound("spawnDrone.wav");
		}
	}

	// this method to spawn new titanium asteroids
	private void spawnTAsteroids(int nNum) {
        for (int nC = 0; nC < nNum; nC++) {
            //Asteroids with size of zero are big
            CommandCenter.getInstance().getOpsList().enqueue(new TitaniumAsteroid(2), CollisionOp.Operation.ADD);

        }
    }
	
	// Level is clear when there is no foes
	private boolean isLevelClear(){
		//if there are no more Asteroids on the screen
		boolean bAsteroidFree = true;
		int num_of_foes = 0;

		for (Movable movFoe : CommandCenter.getInstance().getMovFoes()) {
            num_of_foes += 1;
        }
        if (num_of_foes > 0) {
            bAsteroidFree = false;
        
		}
		
		return bAsteroidFree;

		
	}

	// Determine what is comming up next in higher levels	
	private void checkNewLevel(){
		
		if (isLevelClear() ){
			if (CommandCenter.getInstance().getFalcon() !=null)
				CommandCenter.getInstance().getFalcon().setProtected(true);
			
			// The number of enemies is increasing with the level of the game
			spawnAsteroids(CommandCenter.getInstance().getLevel() + 1);

			if (CommandCenter.getInstance().getLevel() > 1)

				spawnEnemyDrone(CommandCenter.getInstance().getLevel() / 2 );
				spawnTAsteroids(CommandCenter.getInstance().getLevel() / 2 );

			CommandCenter.getInstance().setLevel(CommandCenter.getInstance().getLevel() + 1);

		}
	}


	// Stop game when 2 minutes playing time is up
	private void checkTimeUp(){
	
		if (CommandCenter.getInstance().getElapsedTime() >= 120000){

			CommandCenter.getInstance().setTimeUp(true);
			CommandCenter.getInstance().setPlaying(false);

		}
	}
	
	
	

	// Varargs for stopping looping-music-clips
	private static void stopLoopingSounds(Clip... clpClips) {
		for (Clip clp : clpClips) {
			clp.stop();
		}
	}

	// ===============================================
	// KEYLISTENER METHODS
	// ===============================================

	@Override
	public void keyPressed(KeyEvent e) {

		// Declare falcon and guided missile objects
		Falcon falcon = CommandCenter.getInstance().getFalcon();
		GuidedMissile guidedMissile = CommandCenter.getInstance().getGuidedMissile();

		int nKey = e.getKeyCode();
		// System.out.println(nKey);

		if (nKey == START && !CommandCenter.getInstance().isPlaying())
			startGame();
		
		else {

			switch (nKey) {

				case QUIT:
				System.exit(0);
				break;

				case PAUSE:
				CommandCenter.getInstance().setGamePlot(!CommandCenter.getInstance().isGamePlot());
				// CommandCenter.getInstance().setCurrentElapsedTime();
				
				// if (CommandCenter.getInstance().isPaused())
				// 	stopLoopingSounds(clpMusicBackground, clpThrust);
				// else
				// 	clpMusicBackground.loop(Clip.LOOP_CONTINUOUSLY);
				// break;
			}

		}

		if (falcon != null) {

			switch (nKey) {
			case PAUSE:
				CommandCenter.getInstance().setPaused(!CommandCenter.getInstance().isPaused());
				CommandCenter.getInstance().setCurrentElapsedTime();
				
				if (CommandCenter.getInstance().isPaused())
					stopLoopingSounds(clpMusicBackground, clpThrust);
				else
					clpMusicBackground.loop(Clip.LOOP_CONTINUOUSLY);
				break;
			case QUIT:
				System.exit(0);
				break;
			case UP:
				falcon.thrustOn();
				if (!CommandCenter.getInstance().isPaused())
					clpThrust.loop(Clip.LOOP_CONTINUOUSLY);
				break;
			case LEFT:
				if (CommandCenter.getInstance().isControllingMissile()) {
					guidedMissile.rotateLeft();
				} else {
					falcon.rotateLeft();
				}
		
				break;
			case RIGHT:
				if (CommandCenter.getInstance().isControllingMissile()) {
					guidedMissile.rotateRight();
				} else {
					falcon.rotateRight();
				}
					
				break;
			case SHIELD:
				if (CommandCenter.getInstance().getNumShields() > 0 & !CommandCenter.getInstance().bShieldOn()) {
					CommandCenter.getInstance().getOpsList().enqueue(new FalconShield(), CollisionOp.Operation.ADD);
					CommandCenter.getInstance().setNumShields(CommandCenter.getInstance().getNumShields() - 1);
					CommandCenter.getInstance().setShieldOn(true);
					Sound.playSound("shieldOn.wav");
				}
				break;
			case SPECIAL:

				CommandCenter.getInstance().spawnGuidedMissile();
				CommandCenter.getInstance().setControllingMissile(true);
				break;

			// possible future use
			// case KILL:
			// case NUM_ENTER:

			default:
				break;
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {

		Falcon fal = CommandCenter.getInstance().getFalcon();
		GuidedMissile guidedMissile = CommandCenter.getInstance().getGuidedMissile();

		int nKey = e.getKeyCode();
		 System.out.println(nKey);

		if (fal != null) {

			switch (nKey) {
			case FIRE:
				CommandCenter.getInstance().getOpsList().enqueue(new Bullet(fal), CollisionOp.Operation.ADD);
				Sound.playSound("laser.wav");
				break;

			case MISSILE:
				CommandCenter.getInstance().getOpsList().enqueue(new FalconMissile(fal), CollisionOp.Operation.ADD);
				//Sound.playSound("laser.wav");
				break;
				
			//special is a special weapon, to fire guided missile 
			case SPECIAL:
				CommandCenter.getInstance().setControllingMissile(false);
				// guidedMissile.setExpiretime(75);
				// guidedMissile.stopRotating();
				//Sound.playSound("laser.wav");
				break;
				
			case LEFT:
				fal.stopRotating();
				if (CommandCenter.getInstance().isControllingMissile()) {
					guidedMissile.stopRotating();
				}
			case RIGHT:
				fal.stopRotating();
				if (CommandCenter.getInstance().isControllingMissile()) {
					guidedMissile.stopRotating();
				}
				break;
			case UP:
				fal.thrustOff();
				clpThrust.stop();
				break;
				
			case MUTE:
				if (!bMuted){
					stopLoopingSounds(clpMusicBackground);
					bMuted = !bMuted;
				} 
				else {
					clpMusicBackground.loop(Clip.LOOP_CONTINUOUSLY);
					bMuted = !bMuted;
				}
				break;

            // hyperspace to randomly move the falcon to another location on the screen. 
			case HYPER:
				Point point = new Point(
				Game.R.nextInt((int) Game.DIM.getWidth()),
				Game.R.nextInt((int) Game.DIM.getHeight()));
				CommandCenter.getInstance().getFalcon().setCenter(point);
				break;
				
				
			default:
				break;
			}
		} else {


		}
	}

	@Override
	// Just need it b/c of KeyListener implementation
	public void keyTyped(KeyEvent e) {
	}

}



// Every program has a set of threads where the application logic begins. In standard programs,
// there's only one such thread: the thread that invokes the main method of the program class.
// In applets the initial threads are the ones that construct the applet object and invoke its
// init and start methods; these actions may occur on a single thread, or on two or three
// different threads, depending on the Java platform implementation. In this lesson, we call these
// threads the initial threads.

// In Swing programs, the initial threads don't have a lot to do. Their most essential job is to
// create a Runnable object that initializes the GUI and schedule that object for execution on
// the event dispatch thread. Once the GUI is created, the program is primarily driven by GUI
// events, each of which causes the execution of a short task on the event dispatch thread.
// Application code can schedule additionals tasks on the event dispatch thread (if they complete
// quickly, so as not to interfere with event processing) or a worker thread (for long-running
// tasks).

// An initial thread schedules the GUI creation task by invoking
// javax.swing.SwingUtilities.invokeLater or javax.swing.SwingUtilities.invokeAndWait .
// Both of these methods take a single argument: the Runnable that defines the new task. Their 
// only difference is indicated by their names: invokeLater simply schedules the task and returns; 
// invokeAndWait waits for the task to finish before returning.


