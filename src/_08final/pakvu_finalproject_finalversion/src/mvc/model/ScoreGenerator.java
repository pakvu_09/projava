package mvc.model;

/**
 * Create the interface for Asteroid, Drone to implement.
 */
public interface ScoreGenerator {
    
    int getScore();
}