package mvc.model;

import mvc.controller.Game;
import sounds.Sound;

import java.awt.*;
import java.util.ArrayList;


public class EnemyDrone extends Sprite implements ScoreGenerator{

    private Falcon falcon;    
    private boolean bDrone;
    private int droneHealth;
    private static final int POWER = 10;

    private int score= droneHealth * 100;


    // Declare constructor 
    public EnemyDrone(Falcon fal) {

        super(); 
        falcon = fal;
        setTeam(Team.FOE);
        ArrayList<Point> pntCs = new ArrayList<Point>();

        // Draw shape of a drone
        pntCs.add(new Point(-138, -5));
        pntCs.add(new Point(-126, -11));
        pntCs.add(new Point(-105, -17));
        pntCs.add(new Point(-82, -24));
        pntCs.add(new Point(-54, -27));
        pntCs.add(new Point(-36, -28));
        pntCs.add(new Point(-24, -22));
        pntCs.add(new Point(-9, -16));
        pntCs.add(new Point(2, -17));
        pntCs.add(new Point(17, -22));
        pntCs.add(new Point(24, -29));
        pntCs.add(new Point(34, -32));
        pntCs.add(new Point(48, -32));
        pntCs.add(new Point(63, -27));
        pntCs.add(new Point(78, -23));
        pntCs.add(new Point(98, -18));
        pntCs.add(new Point(110, -14));
        pntCs.add(new Point(125, -3));
        pntCs.add(new Point(112, 0));
        pntCs.add(new Point(97, 3));
        pntCs.add(new Point(81, 4));
        pntCs.add(new Point(68, 5));
        pntCs.add(new Point(51, 7));
        pntCs.add(new Point(29, 10));
        pntCs.add(new Point(20, 17));
        pntCs.add(new Point(11, 21));
        pntCs.add(new Point(-9, 25));
        pntCs.add(new Point(-24, 22));
        pntCs.add(new Point(-33, 15));
        pntCs.add(new Point(-41, 9));
        pntCs.add(new Point(-59, 6));
        pntCs.add(new Point(-83, 4));
        pntCs.add(new Point(-104, 2));
        pntCs.add(new Point(-121, 1));
        pntCs.add(new Point(-137, -5));
        pntCs.add(new Point(119, -4));
        assignPolarPoints(pntCs);

        // Drone will expire after 20000 frames

        setExpire(20000);
        setRadius(50);
        setColor(Color.RED);


        setRandDirection();

        setCenter(new Point(Game.R.nextInt(Game.DIM.width),
                Game.R.nextInt(Game.DIM.height)));
        bDrone = true;

        // Drone health increases with the game level. The higher the level the more powerful enemy is
        
        droneHealth = CommandCenter.getInstance().getLevel() * POWER;
    

    }

    // Declare getter setter for score
    @Override
    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    private void setRandDirection() {
        int nX = Game.R.nextInt(10);
        int nY = Game.R.nextInt(10);

        //set random DeltaX
        if (nX % 2 == 0)
            setDeltaX(nX);
        else
            setDeltaX(-nX);

        //set random DeltaY
        if (nY % 2 == 0)
            setDeltaY(nY);
        else
            setDeltaY(-nY);
    }

    @Override
    public void move() {
        super.move();
        int dX, dY;
        int angle;
        if (getCenter().y - falcon.getCenter().y < 0) {
            dX = getCenter().x - falcon.getCenter().x;
            dY = getCenter().y - falcon.getCenter().y;
            angle = (int) Math.toDegrees(Math.atan2(dY, dX)) - 180;
        } else {
            dX = falcon.getCenter().x - getCenter().x;
            dY = falcon.getCenter().y - getCenter().y;
            angle = (int) Math.toDegrees(Math.atan2(dY, dX));
        }
        setOrientation(angle);


        if (getExpire() % 75 == 0) {
            
            //drone fires bullet, add bullet into OpsList
            CommandCenter.getInstance().getOpsList().enqueue(new DroneWeapon((this)), CollisionOp.Operation.ADD);
            Sound.playSound("droneFire.wav");
            //setRandDirection();
        }
        setExpire(getExpire() - 1);
        
    }

    @Override
    public void draw(Graphics g) {
        super.draw(g);

        //fill this polygon with assigned color in the constructor
        g.fillPolygon(getXcoords(), getYcoords(), dDegrees.length);

        //now draw a white border
        g.setColor(Color.WHITE);
        g.drawPolygon(getXcoords(), getYcoords(), dDegrees.length);
 
    }


    public void setFalcon(Falcon falcon) {
        this.falcon = falcon;
    }


    public boolean isUfoActive() {
        return bDrone;
    }


    public void setUfoActive(boolean ufoActive) {
        this.bDrone = ufoActive;
    }


    public int getDroneHealth() {
        return droneHealth;
    }


    public void setDroneHealth(int level) {
        this.droneHealth = level;
    }





}

