package mvc.model;
import java.awt.*;


// Once launched, the user is temporarily lost control of their falcon, get access to guide the missile instead
public class GuidedMissile extends FalconMissile {

    private final int MAX_EXPIRE = 100;
    private final double FIRE_POWER = 20;
    private boolean bTurningRight = false;
    private boolean bTurningLeft = false;

    final int DEGREE_STEP = 7;
    private int expiredTime = 1;


    // Declare constructor
    public GuidedMissile(Falcon falcon) {
        super(falcon);
        setExpire(MAX_EXPIRE);
        setRadius(20);

        //everything is relative to the falcon ship that fired the bullet
        setDeltaX(falcon.getDeltaX()
                + Math.cos(Math.toRadians(falcon.getOrientation())) * FIRE_POWER);
        setDeltaY(falcon.getDeltaY()
                + Math.sin(Math.toRadians(falcon.getOrientation())) * FIRE_POWER);
        setCenter(falcon.getCenter());

        //set the bullet orientation to the falcon (ship) orientation
        setOrientation(falcon.getOrientation());
        setColor(Color.ORANGE);

    }


    public void rotateLeft() {
        bTurningLeft = true;
    }

    @Override
    public void move() {

        super.move();
        if (CommandCenter.getInstance().isControllingMissile()) {
            double dAdjustX = Math.cos(Math.toRadians(getOrientation()))*FIRE_POWER*.5;
            double dAdjustY = Math.sin(Math.toRadians(getOrientation()))*FIRE_POWER*.5;
            setDeltaX(getDeltaX()*.5+dAdjustX);
            setDeltaY(getDeltaY()*.5+dAdjustY);
        }
        if (bTurningLeft) {

            if (getOrientation() <= 0 && bTurningLeft) {
                setOrientation(360);
            }
            setOrientation(getOrientation() - DEGREE_STEP);
        }
        if (bTurningRight) {
            if (getOrientation() >= 360 && bTurningRight) {
                setOrientation(0);
            }
            setOrientation(getOrientation() + DEGREE_STEP);
        }

        if (getExpire() <= 0) {
            CommandCenter.getInstance().getOpsList().enqueue(this, CollisionOp.Operation.REMOVE);
        } else {
            setExpire(getExpire() - expiredTime);
        }


    }


    public void rotateRight() {
        bTurningRight = true;
    }


    public void stopRotating() {
        bTurningRight = false;
        bTurningLeft = false;
    }

    @Override
    public void draw(Graphics g) {


        if (getExpire() < MAX_EXPIRE - 25)
            colorDrawing(g);
        else {
            drawAlt(g);
        }

    }


    public void setExpiretime(int expiretime) {
        this.expiredTime = expiretime;
    }
}

