package mvc.model;

import java.awt.*;

public class Debris extends Sprite {
    
    private double distance_between_debris;
    final int radius_parameter = 100;

    // private Point center;
    private double angle;
    private int size;

    public Debris(Sprite explodingObejct, double angle) {

        super();
        this.angle = angle;
        setTeam(Team.DEBRIS);
        setExpire(20);
        setRadius(radius_parameter / explodingObejct.getRadius());
        Point explosionLocation = explodingObejct.getCenter();
        this.size =	explodingObejct.getRadius() + 1;

        distance_between_debris = explodingObejct.getRadius() / 10.0;

        // Calculate movement of debris
        setDeltaX(distance_between_debris * Math.cos(Math.toRadians(angle)));
        setDeltaY(distance_between_debris * Math.sin(Math.toRadians(angle)));
        setCenter(new Point((int) explosionLocation.getX(), (int) explosionLocation.getY()));

        // Assign red color for falcon debris, the rest yellow
        if (explodingObejct instanceof Falcon)
            setColor(Color.RED);
        else
            setColor(Color.YELLOW);
    }

    @Override
    public void move() {
        super.move();
        if (getExpire() == 0) {
            CommandCenter.getInstance().getOpsList().enqueue(this, CollisionOp.Operation.REMOVE);
        } else {
            setExpire(getExpire() - 1);
        }
    }

    @Override
    public void draw(Graphics g) {
        g.setColor(getColor());
        g.fillOval((int) (getCenter().getX() - getRadius()), (int) (getCenter().getY() - getRadius()), 2 * getRadius(), 2 * getRadius());
    }


    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}

