package mvc.model;

import sounds.Sound;

import java.util.ArrayList;
import java.util.List;


public class CommandCenter {

	private  int nNumFalcon;
	private  int nLevel;
	private  long lScore;
	private  Falcon falShip;
	private  boolean bPlaying;
	private  boolean bPaused;
	private  boolean bGamePlot;


	// Added fields

	private long levelEpochTime;
	private long gameEpochTime;
	private int numShields;
	private GuidedMissile guidedMissile;

	public long currentElapsedTime;

	
	private boolean bshieldOn = false;
	private boolean bDrone = false;
	private boolean bControllingMissile;
	private boolean bTimeUp = false;
	private boolean gameInitiated;
	private boolean gameTimedOut;
	private boolean gameCleared;
	
	// These ArrayLists with capacities set
	private List<Movable> movDebris = new ArrayList<Movable>(300);
	private List<Movable> movFriends = new ArrayList<Movable>(100);
	private List<Movable> movFoes = new ArrayList<Movable>(200);
	private List<Movable> movFloaters = new ArrayList<Movable>(50);

	private GameOpsList opsList = new GameOpsList();


	private static CommandCenter instance = null;

	// Constructor made private - static Utility class only
	private CommandCenter() {}


	public static CommandCenter getInstance(){
		if (instance == null){
			instance = new CommandCenter();
		}
		return instance;
	}


	public  void initGame(){
		setLevel(0);
		setScore(0);
		setNumFalcons(3);
		setNumShields(3);
	
		spawnFalcon(true);
		this.levelEpochTime = System.currentTimeMillis();
		this.gameEpochTime = System.currentTimeMillis();
		this.gameInitiated = false;
		this.gameCleared = false;
		CommandCenter.getInstance().setGameInitiated(true);
	}
	
	// The parameter is true if this is for the beginning of the game, otherwise false
	// When you spawn a new falcon, you need to decrement its number
	public  void spawnFalcon(boolean bFirst) {
		if (getNumFalcons() != 0) {
			falShip = new Falcon();

			opsList.enqueue(falShip, CollisionOp.Operation.ADD);
			if (!bFirst)
			    setNumFalcons(getNumFalcons() - 1);
		}
		
		Sound.playSound("falconSpawn.wav");

	}

	public GameOpsList getOpsList() {
		return opsList;
	}

	public void setOpsList(GameOpsList opsList) {
		this.opsList = opsList;
	}

	public  void clearAll(){
		movDebris.clear();
		movFriends.clear();
		movFoes.clear();
		movFloaters.clear();
	}

	public  boolean isPlaying() {
		return bPlaying;
	}

	public  void setPlaying(boolean bPlaying) {
		this.bPlaying = bPlaying;
	}

	public  boolean isPaused() {
		return bPaused;
	}

	public  void setPaused(boolean bPaused) {
		this.bPaused = bPaused;
	}

	public  boolean isGamePlot() {
		return bGamePlot;
	}

	public  void setGamePlot(boolean bGamePlot) {
		this.bGamePlot = bGamePlot;
	}

	
	public  boolean isGameOver() {		//if the number of falcons is zero, then game over
		if (getNumFalcons() == 0) {
			return true;
		}
		return false;
	}

	public  int getLevel() {
		return nLevel;
	}

	public   long getScore() {
		return lScore;
	}

	public  void setScore(long lParam) {
		lScore = lParam;
	}

	public  void setLevel(int n) {
		nLevel = n;
	}

	public  int getNumFalcons() {
		return nNumFalcon;
	}

	public  void setNumFalcons(int nParam) {
		nNumFalcon = nParam;
	}
	
	public  Falcon getFalcon(){
		return falShip;
	}
	
	public  void setFalcon(Falcon falParam){
		falShip = falParam;
	}

	public  List<Movable> getMovDebris() {
		return movDebris;
	}



	public  List<Movable> getMovFriends() {
		return movFriends;
	}



	public  List<Movable> getMovFoes() {
		return movFoes;
	}


	public  List<Movable> getMovFloaters() {
		return movFloaters;
	}

	public long getElapsedTime() {

		// elapsedTime += System.currentTimeMillis() - levelEpochTime ;
		// return System.currentTimeMillis() - levelEpochTime ;
		return System.currentTimeMillis() - levelEpochTime + currentElapsedTime ;
	}

	public long getCurrentElapsedTime(){
		return currentElapsedTime;
	}

	public void setCurrentElapsedTime(){
		currentElapsedTime += System.currentTimeMillis() - levelEpochTime  ;
	}

	public void resetLevelEpochTime() {
		this.levelEpochTime = System.currentTimeMillis();
	}

	public long getTotalElapsedTime() {
		return System.currentTimeMillis() - gameEpochTime;
	}

	public void setGameInitiated(boolean gameInitiated) {
		this.gameInitiated = gameInitiated;
	}

    public int getNumShields() {
        return numShields;
    }

    public void setNumShields(int numShields) {
        this.numShields = numShields;
	}
	
	public boolean bShieldOn() {
        return bshieldOn;
    }


    public void setShieldOn(boolean shieldOn) {
        this.bshieldOn = shieldOn;
	}
	
	public boolean getbDrone() {
        return bDrone;
    }

    public void setBDrone(boolean bDrone) {
        this.bDrone = bDrone;
	}


	public boolean isControllingMissile() {
        return bControllingMissile;
	}
	

    public GuidedMissile getGuidedMissile() {
        return guidedMissile;
    }

    public void setControllingMissile(boolean bControllingMissile) {
        this.bControllingMissile = bControllingMissile;
	}
	
	public void spawnGuidedMissile() {
        guidedMissile = new GuidedMissile(falShip);
        opsList.enqueue(guidedMissile, CollisionOp.Operation.ADD);
        Sound.playSound("missile-launch.wav");

	}
	

	public boolean getTimeUp() {
		return bTimeUp;
	}

	public void setTimeUp(boolean bTimeUp) {
		this.bTimeUp = bTimeUp;
	}


	public boolean getGameInitiated() {
		return gameInitiated;
	}



	





}
