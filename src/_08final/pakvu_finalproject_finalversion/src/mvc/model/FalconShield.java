package mvc.model;

import mvc.controller.Game;


import java.awt.*;


public class FalconShield extends Sprite {

    // assign shield size and initial health 
    private int shieldSize = CommandCenter.getInstance().getFalcon().getRadius() * 2 + 50;
    private int shieldHealth = 3;

    // Declare constructor
    public FalconShield() {
        super();
        setTeam(Team.FRIEND);
        this.setCenter(CommandCenter.getInstance().getFalcon().getCenter());
        setRadius(shieldSize / 2);
    }

    @Override
    public void move() {
        super.move();
        Point falCen = CommandCenter.getInstance().getFalcon().getCenter();
        this.setCenter(falCen);
    }


    @Override   
    public void draw(Graphics g) {
        g.setColor(Color.white);
        g.drawOval(getCenter().x - shieldSize / 2, getCenter().y - shieldSize / 2, shieldSize, shieldSize);

        drawShieldBar(g);
    }


    public void drawShieldBar(Graphics g) {
        g.setColor(Color.white);
        String stgDisplay = "Shield Power: "+shieldHealth;
        g.drawString(stgDisplay, Game.DIM.width - 110, Game.DIM.height - 110);
        g.drawRect(Game.DIM.width - 100, Game.DIM.height - 100, 82, 20);
        g.setColor(Color.yellow);
        g.fillRect(Game.DIM.width - 99, Game.DIM.height - 99, 8 * shieldHealth, 18);
    }


    public int getShieldHealth() {
        return shieldHealth;
    }


    public void decreaseShieldHealth() {
        shieldHealth = shieldHealth - 1;
    }
}


