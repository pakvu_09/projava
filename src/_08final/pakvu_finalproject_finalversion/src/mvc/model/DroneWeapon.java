package mvc.model;

import java.awt.*;
import java.util.ArrayList;

public class DroneWeapon extends Sprite {

	private final double firePower = 35.0;


    public DroneWeapon(EnemyDrone drone){
		
		super();
	    setTeam(Team.FOE);
		
		//defined the points on a cartesean grid
		ArrayList<Point> pntCs = new ArrayList<Point>();


		pntCs.add(new Point(0, 5));
		pntCs.add(new Point(1, 3));
		pntCs.add(new Point(1, -2));
		pntCs.add(new Point(-1, -2));
		pntCs.add(new Point(-1, 3));

		assignPolarPoints(pntCs);

        //Drone bullet expires after 20 frames
        
	    setExpire(20);
	    setRadius(15);
	    setColor(drone.getColor());

	    //everything is relative to the falcon ship that fired the bullet
	    setDeltaX( drone.getDeltaX() +
	               Math.cos( Math.toRadians( drone.getOrientation() ) ) * firePower );
	    setDeltaY( drone.getDeltaY() +
	               Math.sin( Math.toRadians( drone.getOrientation() ) ) * firePower );
	    setCenter( drone.getCenter() );

	    //set the bullet orientation to the falcon (ship) orientation
	    setOrientation(drone.getOrientation());


	}

	@Override
	public void move(){

		super.move();

		if (getExpire() == 0)
			CommandCenter.getInstance().getOpsList().enqueue(this, CollisionOp.Operation.REMOVE);
		else
			setExpire(getExpire() - 1);

	}

	@Override
	public void draw(Graphics g) {
		super.draw(g);
		colorDrawing(g);
	}
}

