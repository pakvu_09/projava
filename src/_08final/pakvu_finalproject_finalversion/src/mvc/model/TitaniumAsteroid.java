package mvc.model;

import mvc.controller.Game;

import java.awt.*;
import java.util.*;


public class TitaniumAsteroid extends Asteroid {

    private Random mRandom = new Random();
    private int RAD = 25;
    private int tAHealth = 4;
    private Color color;


    // Declare two constructors to initialize titanium asteroids
    public TitaniumAsteroid(int nSize) {
        super(nSize);
        setDeltaX(getDeltaX() * .5);
        setDeltaY(getDeltaY() * .5);
        color = new Color(mRandom.nextInt(256),
                mRandom.nextInt(256),
                mRandom.nextInt(256));
        setColor(color);
    }


    // Declare constructor
    public TitaniumAsteroid(TitaniumAsteroid astExploded) {

        super(astExploded);

        setTeam(Team.FOE);
        int nSizeNew = astExploded.getSize() + 1;

        int nSpin = Game.R.nextInt(10);
        if (nSpin % 2 == 0)
            nSpin = -nSpin;
        setSpin(nSpin);

        int nDX = Game.R.nextInt(10 + nSizeNew * 2);
        if (nDX % 2 == 0)
            nDX = -nDX;
        setDeltaX(nDX);

        int nDY = Game.R.nextInt(10 + nSizeNew * 2);
        if (nDY % 2 == 0)
            nDY = -nDY;
        setDeltaY(nDY);

        assignRandomShape();
        setRadius(RAD * nSizeNew * 2);
        setCenter(astExploded.getCenter());
        color = new Color(mRandom.nextInt(256),
                mRandom.nextInt(256),
                mRandom.nextInt(256));
        tAHealth = tAHealth * (getSize() + 1);
  
        setColor(color);
    }

    @Override
    public void draw(Graphics g) {

        super.draw(g);        
        colorDrawing(g);
        drawHealth(g);
    }


    public void drawHealth(Graphics g) {
        
        g.setColor(Color.white);
        int radius = getRadius();
        int x = (int) getCenter().getX() - radius;
        int y = (int) getCenter().getY() - radius;

        g.drawRect(x - 8, y - 8, radius * 2, 10);
        g.setColor(Color.white);
        int scale = (int) ((tAHealth * 1.0) / (tAHealth * 1.0) * radius * 2);
        String stgDisplay = "Health: " + tAHealth;
        g.drawString(stgDisplay, x - 6, y - 10);
        g.setColor(Color.gray);
        g.fillRect(x - 7, y - 7, scale, 8);
        
    }

    public int getSize() {

        int nReturn = 0;

        switch (getRadius()) {
            case 25:
                nReturn = 0;
                break;
            case 50:
                nReturn = 1;
                break;
            case 100:
                nReturn = 2;
                break;
            case 150:
                nReturn = 3;
        }
        return nReturn;

    }


    public int getTAHealth() {
        return tAHealth;
    }


    public void setTAHealth(int health) {
        this.tAHealth = health;
    }

    public int getScore() {
        switch (getSize()) {
            case 0:
                return 50;
            case 1:
                return 100;
            case 2:
                return 250;
            case 3:
                return 500;

            default:
                return 0;
        }
    }

}
