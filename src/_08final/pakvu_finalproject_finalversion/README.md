Important:

If the game can not load the header and background pictures. Please make sure you are in a correct working directory as the link the header and background are:

GamePanel line 75 and 90
image = ImageIO.read(new File("./src/images/background.png"));

image = ImageIO.read(new File("./src/images/asteroids-header.jpg"));

I also includes screenshot how the game looks like in my computer as your reference.



Here is the list of new features I implement into the base code:

1. Debris: implemented debris with different color and trajectories after collision
2. Implement scoring system by creating an interface ScoreGeneration. Objects implement this interface will generate different scores when destroyed.
3. Modify code to increase the number of falcons by one whenever it hit NewShipFloater
4. Implement shield 
5. Enemy drones (UFO) and enemy weapon. Different falcon weapons can have different impact on the drones. 

I modified the killFoes method into handleFoes to reflect these changes.

6. Add background for better visual effect.
7. Special weapons: I implement the guided missile, while launching you can control its trajectories but losing falcon control
8. Implementing Hyperspace: allow a falcom to move to a random location
9. Draw health bar, falcon numbers and falcon shields on the top of the game. 
10. Game play: I track the elapsed time since you start the game, each player will have 2 minutes to score a highest points.
When you press pause the time will be freeze.

11. TitaniumAsteroid which appears at higher levels, has a special color, requires several hits before it explodes, changes
color as it weakens, and spawns smaller versions of itself like Asteroid.

I create a TitaniumAsteroid class which inherit from Asteroid class to reflect this changes.

