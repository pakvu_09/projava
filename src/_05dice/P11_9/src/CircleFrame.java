import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.*;

public class CircleFrame extends JFrame {

    private static final int FRAME_WIDTH = 500;
    private static final int FRAME_HEIGHT = 500;
    private CircleComponent scene;
    private boolean is_Center = true;
    private int centerX;
    private int centerY;

    class MousePressListener implements MouseListener {

        public void mousePressed(MouseEvent event) {
            if (is_Center) {
                // first click
                // save the center position
                centerX = event.getX();
                centerY = event.getY();
                is_Center = false;
            } else {
                // second click
                int x = event.getX();
                int y = event.getY();
                // compute the radius
                int radius = (int) Math.sqrt((x - centerX) * (x - centerX) + (y - centerY) * (y - centerY));
                // plot the circle
                scene.moveCircleTo(centerX, centerY, radius);
                is_Center = true;
            }
        }

        public void mouseReleased(MouseEvent event) {
        }

        public void mouseClicked(MouseEvent event) {
        }

        public void mouseEntered(MouseEvent event) {
        }

        public void mouseExited(MouseEvent event) {
        }
    }

    public CircleFrame() {
        scene = new CircleComponent();
        add(scene);
        MouseListener listener = new MousePressListener();
        scene.addMouseListener(listener);
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
    }
}
