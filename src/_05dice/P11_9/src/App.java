import javax.swing.*;

public class App {
    public static void main(String[] args) {

        // create a circle object
        JFrame frame = new CircleFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
