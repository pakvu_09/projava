import javax.swing.*;

public class Driver {
    public static void main(String[] args) {
        // draw German flag
        JFrame frame = new JFrame(); // create a frame object
        frame.setSize(500, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        German comp = new German(); // create a germany flag object
        frame.add(comp);
        frame.setVisible(true);

        // draw Hungarian flag
        JFrame frame2 = new JFrame(); // create a frame object
        frame2.setSize(500, 500);
        frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Hungarian comp2 = new Hungarian(); // create a hungarian flag object
        frame2.add(comp2);
        frame2.setVisible(true);
    }
}
