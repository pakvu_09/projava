import javax.swing.*;

public class App {
    public static void main(String[] args) {
        RestaurantBill frame = new RestaurantBill(); // set up frame
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
