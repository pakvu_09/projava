import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

public class RestaurantBill extends JFrame implements ActionListener {
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JButton button4;
    private JButton button5;
    private JButton button6;
    private JButton button7;
    private JButton button8;
    private JButton button9;
    private JButton button10;
    private JButton button11;

    private final double button1_price = 9.99;
    private final String button1_name = "Bacon Cheese Burger";
    private final double button2_price = 16.99;
    private final String button2_name = "Smoked Salmon";
    private final double button3_price = 13.99;
    private final String button3_name = "Chicken Pasta";
    private final double button4_price = 11.99;
    private final String button4_name = "Roasted Turkey";
    private final double button5_price = 9.99;
    private final String button5_name = "Eggs Your Way";
    private final double button6_price = 13.99;
    private final String button6_name = "Duck Confit Bowl";
    private final double button7_price = 15.99;
    private final String button7_name = "Smoked Ham & Omelet";
    private final double button8_price = 13.99;
    private final String button8_name = "Avocado Toast";
    private final double button9_price = 5.99;
    private final String button9_name = "Ginger Sunrise";
    private final double button10_price = 5.99;
    private final String button10_name = "Melon Berry Cooler";

    private JLabel rateLabel1;
    private JLabel rateLabel2;
    private JLabel rateLabel3;
    private JLabel rateLabel4;
    private JLabel rateLabel5;
    private JLabel rateLabel6;
    private JLabel rateLabel7;
    private JLabel rateLabel8;
    private JLabel rateLabel9;
    private JLabel rateLabel10;
    private JLabel rateLabel11;
    private JLabel rateLabel12;
    private JTextArea resultLabel;
    private JTextField field1;
    private JTextField field2;
    private float totalPrice;

    private final int x_coordinate_begin = 100;
    private final int x_coordinate_end = 310;
    private final int y_coordinate = 50;
    private final int offset = 20;

    public RestaurantBill() {
        createTextLabel(); // add text labels
        createButton(); // add buttons
        createTextField(); // add text field to show the result

        setSize(600, 1500); // set up the frame size
        setLayout(null); // no layout
    }

    private void createTextField() {
        // add text field
        field1 = new JTextField("");
        field1.setBounds(x_coordinate_begin + 40, 11 * y_coordinate, 50, 50);
        field1.setActionCommand("textField1");
        add(field1);

        field2 = new JTextField("");
        field2.setBounds(x_coordinate_begin + 160, 11 * y_coordinate, 50, 50);
        field2.setActionCommand("textField2");
        add(field2);
    }

    private void createTextLabel() {

        // add labels
        rateLabel1 = new JLabel(button1_name + " ------------ " + button1_price);
        rateLabel1.setBounds(x_coordinate_begin, y_coordinate, x_coordinate_end, y_coordinate);
        rateLabel2 = new JLabel(button2_name + " ------------ " + button2_price);
        rateLabel2.setBounds(x_coordinate_begin, 2 * y_coordinate, x_coordinate_end, y_coordinate);
        rateLabel3 = new JLabel(button3_name + " ------------ " + button3_price);
        rateLabel3.setBounds(x_coordinate_begin, 3 * y_coordinate, x_coordinate_end + offset, y_coordinate);
        rateLabel4 = new JLabel(button4_name + " ------------ " + button4_price);
        rateLabel4.setBounds(x_coordinate_begin, 4 * y_coordinate, x_coordinate_end + offset, y_coordinate);
        rateLabel5 = new JLabel(button5_name + " ------------ " + button5_price);
        rateLabel5.setBounds(x_coordinate_begin, 5 * y_coordinate, x_coordinate_end + offset, y_coordinate);
        rateLabel6 = new JLabel(button6_name + " ------------ " + button6_price);
        rateLabel6.setBounds(x_coordinate_begin, 6 * y_coordinate, x_coordinate_end + offset, y_coordinate);
        rateLabel7 = new JLabel(button7_name + " ------------ " + button7_price);
        rateLabel7.setBounds(x_coordinate_begin, 7 * y_coordinate, x_coordinate_end + offset, y_coordinate);
        rateLabel8 = new JLabel(button8_name + " ------------ " + button8_price);
        rateLabel8.setBounds(x_coordinate_begin, 8 * y_coordinate, x_coordinate_end + offset, y_coordinate);
        rateLabel9 = new JLabel(button9_name + " ------------ " + button9_price);
        rateLabel9.setBounds(x_coordinate_begin, 9 * y_coordinate, x_coordinate_end + offset, y_coordinate);
        rateLabel10 = new JLabel(button10_name + " ------------ " + button10_price);
        rateLabel10.setBounds(x_coordinate_begin, 10 * y_coordinate, x_coordinate_end + offset, y_coordinate);
        add(rateLabel1);
        add(rateLabel2);
        add(rateLabel3);
        add(rateLabel4);
        add(rateLabel5);
        add(rateLabel6);
        add(rateLabel7);
        add(rateLabel8);
        add(rateLabel9);
        add(rateLabel10);

        rateLabel11 = new JLabel("Dish:");
        rateLabel11.setBounds(x_coordinate_begin, 11 * y_coordinate, 40, 50);
        rateLabel12 = new JLabel("Price($):");
        rateLabel12.setBounds(x_coordinate_begin + 100, 11 * y_coordinate, 60, 50);
        add(rateLabel11);
        add(rateLabel12);

        resultLabel = new JTextArea("Total: $ \n Tax: $ \n Suggested Tip: $");
        resultLabel.setBounds(x_coordinate_begin, 12 * y_coordinate, 300, 50);
        add(resultLabel);
    }

    private void createButton() {
        // add buttons
        button1 = new JButton("Add");
        button1.setActionCommand("button1");
        button1.addActionListener(this);
        button1.setBounds(x_coordinate_end + offset, y_coordinate, 100, 50);
        add(button1);

        button2 = new JButton("Add");
        button2.setActionCommand("button2");
        button2.addActionListener(this);
        button2.setBounds(x_coordinate_end + offset, 2 * y_coordinate, 100, 50);
        add(button2);

        button3 = new JButton("Add");
        button3.setActionCommand("button3");
        button3.addActionListener(this);
        button3.setBounds(x_coordinate_end + offset, 3 * y_coordinate, 100, 50);
        add(button3);

        button4 = new JButton("Add");
        button4.setActionCommand("button4");
        button4.addActionListener(this);
        button4.setBounds(x_coordinate_end + offset, 4 * y_coordinate, 100, 50);
        add(button4);

        button5 = new JButton("Add");
        button5.setActionCommand("button5");
        button5.addActionListener(this);
        button5.setBounds(x_coordinate_end + offset, 5 * y_coordinate, 100, 50);
        add(button5);

        button6 = new JButton("Add");
        button6.setActionCommand("button6");
        button6.addActionListener(this);
        button6.setBounds(x_coordinate_end + offset, 6 * y_coordinate, 100, 50);
        add(button6);

        button7 = new JButton("Add");
        button7.setActionCommand("button7");
        button7.addActionListener(this);
        button7.setBounds(x_coordinate_end + offset, 7 * y_coordinate, 100, 50);
        add(button7);

        button8 = new JButton("Add");
        button8.setActionCommand("button8");
        button8.addActionListener(this);
        button8.setBounds(x_coordinate_end + offset, 8 * y_coordinate, 100, 50);
        add(button8);

        button9 = new JButton("Add");
        button9.setActionCommand("button9");
        button9.addActionListener(this);
        button9.setBounds(x_coordinate_end + offset, 9 * y_coordinate, 100, 50);
        add(button9);

        button10 = new JButton("Add");
        button10.setActionCommand("button10");
        button10.addActionListener(this);
        button10.setBounds(x_coordinate_end + offset, 10 * y_coordinate, 100, 50);
        add(button10);

        button11 = new JButton("Add");
        button11.setActionCommand("button11");
        button11.addActionListener(this);
        button11.setBounds(x_coordinate_end + offset, 11 * y_coordinate, 100, 50);
        add(button11);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        DecimalFormat df2 = new DecimalFormat("#.##");
        // process the action
        String name = e.getActionCommand();

        if (name.equals("button1")) {
            totalPrice += button1_price;
        } else if (name.equals("button2")) {
            totalPrice += button2_price;
        } else if (name.equals("button3")) {
            totalPrice += button3_price;
        } else if (name.equals("button4")) {
            totalPrice += button4_price;
        } else if (name.equals("button5")) {
            totalPrice += button5_price;
        } else if (name.equals("button6")) {
            totalPrice += button6_price;
        } else if (name.equals("button7")) {
            totalPrice += button7_price;
        } else if (name.equals("button8")) {
            totalPrice += button8_price;
        } else if (name.equals("button9")) {
            totalPrice += button9_price;
        } else if (name.equals("button10")) {
            totalPrice += button10_price;
        } else if (name.equals("button11")) {
            String data = field2.getText();
            double price = Double.parseDouble(data);
            totalPrice += price;
        }

        resultLabel.setText("Total: $" + df2.format(totalPrice) + "\n Tax: $" + df2.format(totalPrice * 0.1)
                + "\n Suggested Tip: $" + df2.format(totalPrice * 0.15));

    }
}
